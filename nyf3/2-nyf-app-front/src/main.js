import { createApp } from 'vue'
import '/@/index.css'
import App from '/@/App.vue'

import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.esm.min.js'
import { BootstrapIconsPlugin } from 'bootstrap-icons-vue'

// import { store } from '/@store/index'
import { router } from '/@router/router'

const app = createApp(App)
// app.use(store)
app.use(router)
app.use(BootstrapIconsPlugin)
app.mount('#app')
