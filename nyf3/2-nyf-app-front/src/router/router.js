// 라우터 인스턴스 생성
import { createRouter, createWebHistory } from 'vue-router'
import routes from '/@router/routes'
import board from '/@router/routing/board'

export const router = createRouter({
    history: createWebHistory(),
    linkActiveClass: 'active',  // 'active' : 활성화된 링크에 대해서 Bootstrap 이 미리 정의한 CSS 를 적용하라
    routes,
    board
})
