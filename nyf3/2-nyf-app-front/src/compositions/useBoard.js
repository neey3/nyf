import { reactive, computed } from 'vue'

export const useBoard = () => {
    const state = reactive({ boards: []})
    const board_count = computed(() => state.boards.length)

    const SET_DATA = (data) => {
        state.boards = data
        console.log('state.boards ### ', state.boards)
    }
    const boards = (filter = null) => {
        if (filter) {
            return computed(() => state.boards.filter((i) => i.title == filter))
        }
        return computed(() => state.boards)
    }
    const setBoards = (data) => {
        if (data.length > 0) {
            SET_DATA(data)
        }
    }
    const getBoardDetail = () => {

    }
    return {
        boards,
        board_count,
        setBoards,
        getBoardDetail
    }
}
