package com.example.nyfappback.api.domain.entity.adm;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "")
public class MyResume {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "제목")
    @Column(name = "title")
    private String title;

    @ApiModelProperty(value = "내용")
    @Column(name = "contents")
    private String contents;

    @ApiModelProperty(value = "등록일")
    @Column(name = "rgdt")
    private LocalDateTime rgdt;

    @ApiModelProperty(value = "수정일")
    @Column(name = "updt")
    private LocalDateTime updt;
}
