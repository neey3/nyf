package com.example.nyfappback.api.repository.adm.admin;

import com.example.nyfappback.api.domain.entity.adm.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AdminRepository extends JpaRepository<Admin, Long>, AdminCustomRepository {
    Optional<Admin> findById(String id);
    Optional<Admin> findByIdx(Long idx);
}
