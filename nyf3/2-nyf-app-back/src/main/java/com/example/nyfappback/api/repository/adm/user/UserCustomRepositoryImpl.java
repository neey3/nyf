package com.example.nyfappback.api.repository.adm.user;

import com.example.nyfappback.api.common.util.ValidUtil;
import com.example.nyfappback.api.domain.entity.adm.User;
import com.example.nyfappback.api.model.adm.user.QRespUserDetail;
import com.example.nyfappback.api.model.adm.user.RespUserDetail;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.example.nyfappback.api.domain.entity.adm.QUser.user;

@RequiredArgsConstructor
public class UserCustomRepositoryImpl implements UserCustomRepository {
    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public QueryResults<RespUserDetail> findUserInfo() {
        return jpaQueryFactory
                .select(Projections.bean(
                        RespUserDetail.class,
                        user.userIdx,
                        user.userId,
                        user.userName,
                        user.userEmail,
                        user.userPhone,
                        user.withdrawalStatus,
                        user.gender,
                        user.withdrawalDt,
                        user.rgdt,
                        user.lastActionTime
                ))
                .from(user)
                .orderBy(user.userIdx.asc())
                .fetchResults()
                ;
    }

    @Override
    public List<User> findUserListByIdxList(List<Long> idxList) {
        return jpaQueryFactory
                .selectFrom(user)
                .where(user.userIdx.in(idxList))
                .fetch();
    }

    @Override
    public RespUserDetail findUserDetail(ReqIdx body) {
        return jpaQueryFactory
                .select(new QRespUserDetail(
                        user.userIdx,
                        user.userId,
                        user.userName,
                        user.userEmail,
                        user.userPhone,
                        user.channel,
                        user.withdrawalStatus,
                        user.gender,
                        user.rgdt,
                        user.updt,
                        user.withdrawalDt,
                        user.lastActionTime,
                        user.userAddress.postalCode,
                        user.userAddress.addr1,
                        user.userAddress.addr2
                ))
                .from(user)
                .where(eqUserIdx(body.getIdx()))
                .fetchOne();
    }

    @Override
    public User findUserByUserIdx(Long userIdx) {
        return jpaQueryFactory
                .selectFrom(user)
                .where(eqUserIdx(userIdx))
                .fetchOne()
                ;
    }

    BooleanExpression eqUserIdx(Long userIdx) {
        return ValidUtil.isNullZero(userIdx) ? null : user.userIdx.eq(userIdx);
    }

}
