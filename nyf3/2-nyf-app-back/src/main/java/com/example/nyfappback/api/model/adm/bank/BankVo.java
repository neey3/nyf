package com.example.nyfappback.api.model.adm.bank;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class BankVo {
    private String  code;
    private String  bankName;
}
