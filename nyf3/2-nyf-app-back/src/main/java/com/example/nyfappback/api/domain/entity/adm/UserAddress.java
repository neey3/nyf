package com.example.nyfappback.api.domain.entity.adm;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class UserAddress {
    @Column(name = "postal_code")
    private String postalCode;

    @Column(name = "addr1")
    private String addr1;

    @Column(name = "addr2")
    private String addr2;
}
