package com.example.nyfappback.api.common.dto;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class JwtToken {
    private String id;
    private Long idx;
}
