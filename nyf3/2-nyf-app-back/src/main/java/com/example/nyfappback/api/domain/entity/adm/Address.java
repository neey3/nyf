package com.example.nyfappback.api.domain.entity.adm;

import com.example.nyfappback.api.model.adm.address.ReqAddressInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Slf4j
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "address")
@Builder
@DynamicInsert
@DynamicUpdate
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "addr_idx")
    @ApiModelProperty(value = "주소지 IDX")
    private Long addrIdx;

//    @ApiModelProperty(value = "회원 IDX")
//    @Column(name = "user_id")
//    private Long userIdx;

    @ApiModelProperty(value = "주소1")
    @Column(name = "addr1")
    private String addr1;

    @ApiModelProperty(value = "주소2")
    @Column(name = "addr2")
    private String addr2;

    @ApiModelProperty(value = "우편번호")
    @Column(name = "postal_code")
    private String postalCode;

    @ApiModelProperty(value = "기본배송지 여부 (1 기본, 0 기타)")
    @Column(name = "is_default_addr")
    private Integer isDefaultAddr;

    @ApiModelProperty(value = "주소지 별칭")
    @Column(name = "addr_nickname")
    private String addrNickname;

    @ApiModelProperty(value = "받는분 이름")
    @Column(name = "recipient_name")
    private String recipientName;

    @ApiModelProperty(value = "받는분 핸드폰")
    @Column(name = "recipient_phone")
    private String recipientPhone;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name="user_idx")
    private User user;


    /**
     * 등록
     */
    public static Address createAddress(ReqAddressInfo addr, User user) {
        return Address
                .builder()
                .user(user)
                .addr1(addr.getAddr1())
                .addr2(addr.getAddr2())
                .postalCode(addr.getPostalCode())
                .isDefaultAddr(addr.getIsDefaultAddr())
                .addrNickname(addr.getAddrNickname())
                .recipientName(addr.getRecipientName())
                .recipientPhone(addr.getRecipientPhone())
                .build();
    }

    /**
     * 수정
     */
    public void modifyAddress(ReqAddressInfo updateInfo) {
        this.addr1 = updateInfo.getAddr1();
        this.addr2 = updateInfo.getAddr2();
        this.postalCode = updateInfo.getPostalCode();
        this.addrNickname = updateInfo.getAddrNickname();
        this.recipientName = updateInfo.getRecipientName();
        this.recipientPhone = updateInfo.getRecipientPhone();
        this.isDefaultAddr = updateInfo.getIsDefaultAddr();
    }

}
