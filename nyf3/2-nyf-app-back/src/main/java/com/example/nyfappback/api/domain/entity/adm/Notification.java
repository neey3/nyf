package com.example.nyfappback.api.domain.entity.adm;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "")
public class Notification {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "내용")
    @Column(name = "contents")
    private String contents;

    @ApiModelProperty(value = "일자")
    @Column(name = "expiration")
    private LocalDateTime expiration;

    @ApiModelProperty(value = "타입 -enum(WARNING, NOTHING)")
    @Column(name = "type")
    private String type;
}
