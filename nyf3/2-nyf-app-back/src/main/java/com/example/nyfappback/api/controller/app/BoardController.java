package com.example.nyfappback.api.controller.app;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RequestMapping(value = "/board")
@RestController
@RequiredArgsConstructor
public class BoardController {
}
