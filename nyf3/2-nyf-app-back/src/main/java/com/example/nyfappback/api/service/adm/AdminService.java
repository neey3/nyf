package com.example.nyfappback.api.service.adm;

import com.example.nyfappback.api.common.dto.JwtToken;
import com.example.nyfappback.api.common.dto.NyfResult;
import com.example.nyfappback.api.common.service.CommonService;
import com.example.nyfappback.api.common.type.ErrorType;
import com.example.nyfappback.api.common.util.ValidUtil;
import com.example.nyfappback.api.domain.entity.adm.Admin;
import com.example.nyfappback.api.model.adm.admin.ReqAdminInfo;
import com.example.nyfappback.api.model.adm.admin.RespAdminDetail;
import com.example.nyfappback.api.model.adm.admin.RespAdminList;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.example.nyfappback.api.repository.adm.admin.AdminRepository;
import com.example.nyfappback.common.config.CustomModelMapperConfig;
import com.example.nyfappback.common.exception.BaseException;
import com.example.nyfappback.common.exception.NotFoundException;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class AdminService {
    private final CommonService commonService;
    private final CustomModelMapperConfig customModelMapper;
    private final AdminRepository adminRepository;

    /**
     * 목록 조회
     */
    public NyfResult findAdminList(HttpServletRequest req) {
        NyfResult result = NyfResult.getDefResult();

        QueryResults<RespAdminList> adminList = adminRepository.findAllAdmin();

        result.addObject("result", true);
        result.addObject("data", adminList.getResults());
        result.addObject("totalCnt", adminList.getTotal());

        return result;
    }

    /**
     * 상세 조회
     */
    public NyfResult findAdminDetail(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();

        ModelMapper mapper = customModelMapper.strictMapper();
        JwtToken adminInfo = CommonService.getMngLoginInfo();
        log.info("[ADMIN] findAdminDetail ## admin_id : {}", adminInfo.getId());

        Admin admin = adminRepository.findByIdx(body.getIdx()).orElse(null);

        if (ObjectUtils.isEmpty(admin)) {
            throw new BaseException("존재하지 않는 관리자입니다. IDX 를 확인해 주세요.");
//            result.setHttpStatus(HttpStatus.BAD_REQUEST);
//            return result;
        }

        result.addObject("data", mapper.map(admin, RespAdminDetail.class));
        result.addObject("result", true);

        return result;
    }

    /**
     * 관리자 등록
     */
    @Transactional
    public NyfResult registerAdmin(HttpServletRequest req, ReqAdminInfo body) {
        NyfResult result = NyfResult.getDefResult();
        JwtToken adminInfo = CommonService.getMngLoginInfo();
        log.info("[registerAdmin] admin_id : " + adminInfo.getId());

        if (adminRepository.findById(body.getId()).isPresent()) {
            result.addObject("result", false);
            result.addObject("errMsg", "이미 사용중인 ID 입니다.");
            return result;
        }

        Admin insertAdmin = Admin.createAdmin(body);
        adminRepository.save(insertAdmin);

        result.addObject("result", true);
        result.addObject("msg", "등록 되었습니다.");
        return result;
    }

    /**
     * 관리자 수정
     */
    @Transactional
    public NyfResult modifyAdmin(HttpServletRequest req, ReqAdminInfo body) {
        NyfResult result = NyfResult.getDefResult();
        JwtToken adminInfo = CommonService.getMngLoginInfo();
        log.info("[modifyAdmin] admin_id : " + adminInfo.getId());

        if (ValidUtil.isNullZero(body.getIdx())) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        Admin admin = adminRepository.findByIdx(body.getIdx()).orElseThrow(() -> new NotFoundException(ErrorType.BAD_REQUEST_ERROR));
        admin.modifyAdmin(body);

        result.addObject("result", true);
        result.addObject("msg", "수정 되었습니다.");

        return result;
    }

    /**
     * 관리자 삭제
     */
    @Transactional
    public NyfResult deleteAdmin(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();
        JwtToken adminInfo = CommonService.getMngLoginInfo();
        log.info("[deleteAdmin] admin_id : " + adminInfo.getId());

        List<Admin> adminList = adminRepository.findAdminListByIdxList(body.getIdxList());

        if (ObjectUtils.isEmpty(adminList)) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        adminList.forEach(adminRepository::delete);

        result.addObject("result", true);
        result.addObject("msg", "삭제 되었습니다.");

        return result;
    }

}
