package com.example.nyfappback.api.common.type;

import com.querydsl.core.types.dsl.NumberPath;
import lombok.Getter;

public class UserType {
    @Getter
    public enum UserStatusEnum {
        WITHDRAW(0, "탈퇴회원"),
        REGISTERED (1, "가입회원");

        private Integer type;
        private String userStatusValue;

        UserStatusEnum(Integer type, String userStatusValue) {
            this.type = type;
            this.userStatusValue = userStatusValue;
        }

        public static String getTypeToUserStatusValue(Integer type){
            for(UserStatusEnum statusTypeEnum : UserStatusEnum.values()){
                if(statusTypeEnum.getType() == type){
                    return statusTypeEnum.getUserStatusValue();
                }
            }
            return "";
        }
        public static String getTypeToUserStatusValue(NumberPath type){
            for(UserStatusEnum statusTypeEnum : UserStatusEnum.values()){
                if(statusTypeEnum.getType() == Integer.parseInt(type.stringValue().toString())){
                    return statusTypeEnum.getUserStatusValue();
                }
            }
            return "";
        }
    }

    @Getter
    public enum WithdrawalCodeEnum {
        PERSONAL_LEAK(1,"개인정보유출"),
        USE_DISS (2,"사이트이용불만"),
        CUSTOMER_DISS (3,"고객서비스불만"),
        PRD_DISS (4,"상품불만"),
        LOW_FREQUENCY(5,"이용빈도저조"),
        ETC (6,"기타"),
        PCWEB(7,"사용자 탈퇴(PCWEB)");

        private Integer type;
        private String codeValue;

        WithdrawalCodeEnum(Integer type, String codeValue) {
            this.type = type;
            this.codeValue = codeValue;
        }

        public static String getTypeToCodeValue(Integer type){
            for(WithdrawalCodeEnum withCodeEnum : WithdrawalCodeEnum.values()){
                if(withCodeEnum.getType() == type){
                    return withCodeEnum.getCodeValue();
                }
            }
            return "";
        }
    }

    @Getter
    public enum FindTypeEnum {
        USERID(1,"아이디"),
        USERPHONE(2,"핸드폰"),
        USERIDX(3,"인덱스");

        private Integer type;
        private String findTypeValue;

        FindTypeEnum(Integer type, String findTypeValue) {
            this.type = type;
            this.findTypeValue = findTypeValue;
        }

        public static String getTypeToFindTypeValue(Integer type){
            for(FindTypeEnum findTypeEnum : FindTypeEnum.values()){
                if(findTypeEnum.getType() == type){
                    return findTypeEnum.getFindTypeValue();
                }
            }
            return "";
        }
    }

    @Getter
    public enum GenderEnum {
        MAN(0,"남자"),
        WOMAN(1,"여자");

        private Integer type;
        private String value;

        GenderEnum(Integer type, String value) {
            this.type = type;
            this.value = value;
        }

        public static String getTypeTValue(Integer type){
            for(GenderEnum genderEnum : GenderEnum.values()){
                if(genderEnum.getType() == type){
                    return genderEnum.getValue();
                }
            }
            return "";
        }

        public static String getTypeTValue(NumberPath type){
            for(GenderEnum genderEnum : GenderEnum.values()){
                if(genderEnum.getType() == Integer.parseInt(type.stringValue().toString())){
                    return genderEnum.getValue();
                }
            }
            return "";
        }
    }

}
