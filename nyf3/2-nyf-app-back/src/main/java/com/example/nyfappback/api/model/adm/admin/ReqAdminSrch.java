package com.example.nyfappback.api.model.adm.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ReqAdminSrch {
    @ApiModelProperty(name = "level", value = "관리자 레벨 (0: 일반관리자  1: 슈퍼관리자)")
    private Integer level;
}
