package com.example.nyfappback.api.common.service;

import com.example.nyfappback.api.common.dto.JwtToken;
import com.example.nyfappback.api.common.dto.NyfResult;
import com.example.nyfappback.api.common.type.UserType;
import com.example.nyfappback.api.common.util.ValidUtil;
import com.example.nyfappback.api.domain.entity.adm.User;
import com.example.nyfappback.api.model.adm.bank.BankVo;
import com.example.nyfappback.api.model.adm.user.UserVo;
import com.example.nyfappback.api.repository.adm.bank.BankRepository;
import com.example.nyfappback.api.repository.adm.user.UserRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommonService {
    private final UserRepository userRepository;
    private final BankRepository bankRepository;

    /**
     * 로그인 관리자 정보 조회
     */
    public static JwtToken getMngLoginInfo() {
        Map<String, Object> adminMap = JwtService.get("admin");
        if (adminMap == null) return null;
        return new ObjectMapper().convertValue(adminMap, JwtToken.class);
    }

    public UserVo isLoginCheck(HttpServletRequest request){
        if (!(Boolean) request.getAttribute("isLogin")) {
            return null;
        }
        return (UserVo) request.getAttribute("userInfo");
    }

    public User getUser(Long userIdx) {
        User user = userRepository.findByUserIdxNative(userIdx);
        if (ValidUtil.isNullZero(userIdx)) return null;
        else return user;
    }

    public User getUserInfo(String id, String password) {
//        User userVo = userRepository.findUserInfo(UserType.FindTypeEnum.USERID.getType(), id);
//        if (userVo == null || userVo.getPassword() == null || userVo.getPassword().length() == 0) {
//            return null;
//        } else {
//            if (userVo.getPassword().equals(password)) return userVo;
//            else return null;
//        }
        return null;
    }

    /**
     * 은행 목록 조회
     */
    public NyfResult findBankList() {
        NyfResult result = NyfResult.getDefResult();

        Map<String, Object> results = new HashMap<>();
        List<BankVo> bankDataList = bankRepository.findBankListVo();

        results.put("BankList", bankDataList);
        result.addObject("result", true);
        result.addObject("data", results);
        result.addObject("info", new ArrayList<>());

        return result;
    }


}
