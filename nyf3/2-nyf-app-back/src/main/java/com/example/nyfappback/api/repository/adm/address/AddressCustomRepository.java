package com.example.nyfappback.api.repository.adm.address;

import com.example.nyfappback.api.domain.entity.adm.Address;
import com.example.nyfappback.api.model.adm.address.RespAddressInfo;
import com.querydsl.core.QueryResults;

import java.util.List;

public interface AddressCustomRepository {
    public QueryResults<RespAddressInfo> findAddrListByUserIdx(Long userIdx);
    public List<Address> findAddrListByIdxList(List<Long> idxList); // addrIdx
}
