package com.example.nyfappback.api.repository.adm.address;

import com.example.nyfappback.api.common.util.ValidUtil;
import com.example.nyfappback.api.domain.entity.adm.Address;
import com.example.nyfappback.api.model.adm.address.QRespAddressInfo;
import com.example.nyfappback.api.model.adm.address.RespAddressInfo;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.example.nyfappback.api.domain.entity.adm.QAddress.address;

@RequiredArgsConstructor
public class AddressCustomRepositoryImpl implements AddressCustomRepository {
    private final JPAQueryFactory jpaQueryFactory;


    @Override
    public QueryResults<RespAddressInfo> findAddrListByUserIdx(Long userIdx) {
        return jpaQueryFactory
                .select(new QRespAddressInfo(
                        address.addrIdx,
                        address.user.userIdx,
                        address.addr1,
                        address.addr2,
                        address.postalCode,
                        address.isDefaultAddr,
                        address.addrNickname,
                        address.recipientName,
                        address.recipientPhone
                ))
                .from(address)
                .where(eqUserIdx(userIdx))
                .orderBy(address.addrIdx.asc())
                .fetchResults();
    }
    BooleanExpression eqUserIdx(Long userIdx) {
        return ValidUtil.isNullZero(userIdx) ? null : address.user.userIdx.eq(userIdx);
    }

    @Override
    public List<Address> findAddrListByIdxList(List<Long> idxList) {
        return jpaQueryFactory
                .selectFrom(address)
                .where(address.addrIdx.in(idxList))
                .fetch();
    }

}
