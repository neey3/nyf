package com.example.nyfappback.api.repository.adm.bank;

import com.example.nyfappback.api.domain.entity.adm.Bank;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

public interface BankRepository extends JpaRepository<Bank, String>, BankCustomRepository {
}
