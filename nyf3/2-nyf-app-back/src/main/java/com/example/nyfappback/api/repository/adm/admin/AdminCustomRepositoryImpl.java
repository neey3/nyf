package com.example.nyfappback.api.repository.adm.admin;

import com.example.nyfappback.api.domain.entity.adm.Admin;
import com.example.nyfappback.api.model.adm.admin.RespAdminList;
import com.querydsl.core.QueryResults;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.example.nyfappback.api.domain.entity.adm.QAdmin.admin;

@RequiredArgsConstructor
public class AdminCustomRepositoryImpl implements AdminCustomRepository {
    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public QueryResults<RespAdminList> findAllAdmin() {
        return jpaQueryFactory
                .select(Projections.bean(
                        RespAdminList.class,
                        admin.idx,
                        admin.id,
                        admin.level,
                        admin.name,
                        admin.rgdt,
                        admin.updt
                ))
                .from(admin)
                .orderBy(admin.idx.asc())
                .fetchResults()
                ;
    }

    @Override
    public List<Admin> findAdminListByIdxList(List<Long> idxList) {
        return jpaQueryFactory
                .selectFrom(admin)
                .where(admin.idx.in(idxList))
                .fetch();
    }


}
