package com.example.nyfappback.common.config;

import com.example.nyfappback.common.DataSourceRead;
import com.example.nyfappback.common.DataSourceType;
import com.example.nyfappback.common.DataSourceWrite;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableAutoConfiguration(exclude = {DataSourceAutoConfiguration.class})
@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = {"com.example.nyfappback"})
@EnableJpaRepositories(basePackages = {"com.example.nyfappback.api", "com.example.nyfappback.common"})
public class RoutingDataSourceConfig {

    @Bean
    public DataSource routingDataSource(@Qualifier("writeDataSource") DataSource writeDataSource,
                                        @Qualifier("readDataSource") DataSource readDataSource) {
        ReplicationRoutingDataSource routingDataSource = new ReplicationRoutingDataSource();

        Map dataSourceMap = new HashMap<>();
        dataSourceMap.put(DataSourceType.WRITE, writeDataSource);
        dataSourceMap.put(DataSourceType.READ, readDataSource);
        routingDataSource.setTargetDataSources(dataSourceMap);
        routingDataSource.setDefaultTargetDataSource(writeDataSource);
        return routingDataSource;
    }

    @Primary
    @Bean
    public DataSource dataSource(@Qualifier("routingDataSource") DataSource routingDataSource) {
        return new LazyConnectionDataSourceProxy(routingDataSource);
    }

    @Bean(name = "writeDataSource")
    public DataSource writeDataSource(DataSourceWrite dataSourceWrite) {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(dataSourceWrite.getUrl());
        hikariDataSource.setUsername(dataSourceWrite.getUsername());
        hikariDataSource.setPassword(dataSourceWrite.getPassword());
        hikariDataSource.setMaximumPoolSize(dataSourceWrite.getMaximumPoolSize());
        hikariDataSource.setMaxLifetime(dataSourceWrite.getMaxLifetime());
        hikariDataSource.setAutoCommit(dataSourceWrite.isAutoCommit());
        return hikariDataSource;
    }

    @Bean(name = "readDataSource")
    public DataSource readDataSource(DataSourceRead dataSourceRead) {
        HikariDataSource hikariDataSource = new HikariDataSource();
        hikariDataSource.setJdbcUrl(dataSourceRead.getUrl());
        hikariDataSource.setUsername(dataSourceRead.getUsername());
        hikariDataSource.setPassword(dataSourceRead.getPassword());
        hikariDataSource.setMaximumPoolSize(dataSourceRead.getMaximumPoolSize());
        hikariDataSource.setMaxLifetime(dataSourceRead.getMaxLifetime());
        hikariDataSource.setAutoCommit(dataSourceRead.isAutoCommit());
        return hikariDataSource;
    }
}
