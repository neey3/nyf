package com.example.nyfappback.api.model.adm.address;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ReqAddressInfo {

//    @ApiModelProperty(name = "addrIdx", value = "주소지 IDX", required = false, hidden = true)
    @ApiModelProperty(value = "주소지 IDX")
    private Long addrIdx;

    @ApiModelProperty(value = "회원 IDX")
    private Long userIdx;

    @ApiModelProperty(value = "주소1")
    private String addr1;

    @ApiModelProperty(value = "주소2")
    private String addr2;

    @ApiModelProperty(value = "우편번호")
    private String postalCode;

    @ApiModelProperty(value = "기본배송지 여부 (1 기본, 0 기타)")
    private Integer isDefaultAddr;

    @ApiModelProperty(value = "주소 별칭")
    private String addrNickname;

    @ApiModelProperty(value = "받는분 이름")
    private String recipientName;

    @ApiModelProperty(value = "받는분 전화번호")
    private String recipientPhone;
}
