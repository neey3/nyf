package com.example.nyfappback.api.repository.adm.address;

import com.example.nyfappback.api.domain.entity.adm.Address;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long>, AddressCustomRepository {
    Address findAddressByAddrIdx(Long addrIdx);
}
