package com.example.nyfappback.api.model.common.response;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RespCommonImg {
    @ApiModelProperty(value="썸네일1")
    private String thumb1;
    @ApiModelProperty(value="썸네일2")
    private String thumb2;
    @ApiModelProperty(value="썸네일3")
    private String thumb3;
    @ApiModelProperty(value="썸네일4")
    private String thumb4;
    @ApiModelProperty(value="썸네일5")
    private String thumb5;
    @ApiModelProperty(value="썸네일6")
    private String thumb6;
    @ApiModelProperty(value="썸네일7")
    private String thumb7;
    @ApiModelProperty(value="썸네일8")
    private String thumb8;
    @ApiModelProperty(value="썸네일9")
    private String thumb9;
    @ApiModelProperty(value="썸네일10")
    private String thumb10;

    @ApiModelProperty(value="이미지")
    private String filename;
    @ApiModelProperty(value="경로")
    private String path;
    @ApiModelProperty(value="너비")
    private Integer width;
    @ApiModelProperty(value="높이")
    private Integer height;
}
