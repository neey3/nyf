package com.example.nyfappback.common;

public enum DataSourceType {
    WRITE, READ
}
