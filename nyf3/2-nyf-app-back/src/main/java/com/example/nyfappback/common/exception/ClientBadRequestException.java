package com.example.nyfappback.common.exception;

import com.example.nyfappback.api.common.type.ErrorType;
import lombok.Getter;

@Getter
public class ClientBadRequestException extends RuntimeException{
    private ErrorType ete;
    private Exception exception;

    public ClientBadRequestException(ErrorType ete){
        super(ete.getErrMsg());
        this.ete = ete;
    }
    public ClientBadRequestException(ErrorType ete, Exception exception) {
        this.ete = ete;
        this.exception = exception;
    }

    public ClientBadRequestException(String message, ErrorType ete, Exception exception) {
        super(message);
        this.ete = ete;
        this.exception = exception;
    }
}
