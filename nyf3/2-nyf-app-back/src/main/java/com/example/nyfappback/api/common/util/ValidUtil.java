package com.example.nyfappback.api.common.util;

import org.springframework.util.ObjectUtils;

public class ValidUtil {
    public static boolean isNullZero(Long i){
        return  ObjectUtils.isEmpty(i) || i.intValue() == 0;
    }
}
