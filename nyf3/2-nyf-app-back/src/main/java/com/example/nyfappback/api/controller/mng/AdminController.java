package com.example.nyfappback.api.controller.mng;

import com.example.nyfappback.api.common.dto.NyfResult;
import com.example.nyfappback.api.model.adm.admin.ReqAdminInfo;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.example.nyfappback.api.service.adm.AdminService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Log4j2
@RequestMapping(value = "/mng/admin")
@RestController
@RequiredArgsConstructor
public class AdminController {
    private final AdminService adminService;


    @ApiOperation(value = "[관리자] 목록 조회", response = NyfResult.class, tags = "ADMIN/관리자")
    @GetMapping("/list")
    public Object findAdminList(HttpServletRequest req) {
        return adminService.findAdminList(req).getResponse();
    }

    @ApiOperation(value = "[관리자] 상세 조회", response = NyfResult.class, tags = "ADMIN/관리자")
    @PostMapping("/detail")
    public Object findAdminDetail(HttpServletRequest req, @RequestBody ReqIdx body) {
        return adminService.findAdminDetail(req, body).getResponse();
    }

    @ApiOperation(value = "[관리자] 등록", response = Map.class, tags = "ADMIN/관리자")
    @PostMapping("/register")
    public Object registerAdmin(HttpServletRequest req, @RequestBody ReqAdminInfo body) {
        return adminService.registerAdmin(req, body).getResponse();
    }

    @ApiOperation(value = "[관리자] 수정", response = Map.class, tags = "ADMIN/관리자")
    @PostMapping("/modify")
    public Object modifyAdmin(HttpServletRequest req, @RequestBody ReqAdminInfo body) {
        return adminService.modifyAdmin(req, body).getResponse();
    }

    @ApiOperation(value = "[관리자] 삭제", response = Map.class, tags = "ADMIN/관리자")
    @PostMapping("/delete")
    public Object deleteAdmin(HttpServletRequest req, @RequestBody ReqIdx body) {
        return adminService.deleteAdmin(req, body).getResponse();
    }


}
