package com.example.nyfappback.api.repository.adm.user;

import com.example.nyfappback.api.domain.entity.adm.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserCustomRepository {

    @Query("SELECT usr FROM User usr WHERE usr.userIdx = :userIdx")
    public User findByUserIdxNative(Long userIdx);

    Optional<User> findByUserIdx(Long idx);
}
