package com.example.nyfappback.api.model.adm.address;

import com.querydsl.core.annotations.QueryProjection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class RespAddressInfo {

    @ApiModelProperty(value = "주소지 IDX")
    private Long addrIdx;

    @ApiModelProperty(value = "회원 IDX")
    private Long userIdx;

    @ApiModelProperty(value = "주소1")
    private String addr1;

    @ApiModelProperty(value = "주소2")
    private String addr2;

    @ApiModelProperty(value = "우편번호")
    private String postalCode;

    @ApiModelProperty(value = "기본배송지 여부 (1 기본, 0 기타)")
    private Integer isDefaultAddr;

    @ApiModelProperty(value = "주소지 별칭")
    private String addrNickname;

    @ApiModelProperty(value = "받는분 이름")
    private String recipientName;

    @ApiModelProperty(value = "받는분 핸드폰")
    private String recipientPhone;


    @QueryProjection
    public RespAddressInfo(Long addrIdx, Long userIdx, String addr1, String addr2, String postalCode, Integer isDefaultAddr, String addrNickname, String recipientName, String recipientPhone) {
        this.addrIdx = addrIdx;
        this.userIdx = userIdx;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.postalCode = postalCode;
        this.isDefaultAddr = isDefaultAddr;
        this.addrNickname = addrNickname;
        this.recipientName = recipientName;
        this.recipientPhone = recipientPhone;
    }
}
