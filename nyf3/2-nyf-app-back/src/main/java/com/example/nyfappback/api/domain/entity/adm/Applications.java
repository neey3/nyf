package com.example.nyfappback.api.domain.entity.adm;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "applications")
public class Applications {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "이름")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "내용")
    @Column(name = "content")
    private String contents;

    @ApiModelProperty(value = "츨시일")
    @Column(name = "date")
    private LocalDateTime date;

    @ApiModelProperty(value = "플랫폼")
    @Column(name = "platform")
    private String platform;

    @ApiModelProperty(value = "URL")
    @Column(name = "url")
    private String url;

    @ApiModelProperty(value = "첨부 이미지")
    @Column(name = "image")
    private Integer image;
}
