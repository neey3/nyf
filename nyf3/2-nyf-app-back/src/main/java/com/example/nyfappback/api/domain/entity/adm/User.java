package com.example.nyfappback.api.domain.entity.adm;

import com.example.nyfappback.api.model.adm.user.ReqUserInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "user",
       indexes = {
            @Index(name = "USER_ID", columnList = "user_id", unique = true)
       }
)
@Builder
@DynamicInsert
@DynamicUpdate
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_idx")
    @ApiModelProperty(value = "회원 IDX")
    private Long userIdx;

    @ApiModelProperty(value = "아이디")
    @Column(name = "user_id")
    private String userId;

    @ApiModelProperty(value = "이름")
    @Column(name = "user_name")
    private String userName;

    @ApiModelProperty(value = "비밀번호")
    @Column(name = "password")
    private String password;

    @ApiModelProperty(value = "이메일")
    @Column(name = "user_email")
    private String userEmail;

    @ApiModelProperty(value = "전화번호")
    @Column(name = "user_phone")
    private String userPhone;

    @ApiModelProperty(value = "유입경로 (PC, APP)")
    @Column(name = "channel")
    private String channel;

    @ApiModelProperty(value = "로그인 key")
    @Column(name = "enc_key")
    private String encKey;

    @ApiModelProperty(value = "가입 상태 (0 탈퇴, 1 가입중)")
    @Column(name = "withdrawal_status")
    private Integer withdrawalStatus;

    @ApiModelProperty(value = "성별 (0 남자, 1 여자)")
    @Column(name = "gender")
    private String gender;

    @ApiModelProperty(value = "가입일자")
    @Column(name = "rgdt")
    private LocalDateTime rgdt;

    @ApiModelProperty(value = "수정일자")
    @Column(name = "updt")
    private LocalDateTime updt;

    @ApiModelProperty(value = "탈퇴일자")
    @Column(name = "withdrawal_dt")
    private LocalDateTime withdrawalDt;

    @ApiModelProperty(value = "마지막 사용일자")
    @Column(name = "last_action_time")
    private LocalDateTime lastActionTime;

    @Embedded
    UserAddress userAddress;

    @OneToMany(mappedBy = "user")
    private List<Address> addrList;
//    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
//    private List<Address> addrList = new ArrayList<>();


    public void setLoginJwtEncKey(String encKey, LocalDateTime lastActionTime) {
        this.encKey = encKey;
        this.lastActionTime = lastActionTime;
    }

    public void setUserIdx(Long userIdx) {
        this.userIdx = userIdx;
    }


    /**
     * 등록을 위한 Entity 생성
     */
    public static User createUser(ReqUserInfo body) {
        return User
                .builder()
                .userId(body.getUserId())
                .userName(body.getUserName())
                .password(body.getPassword())
                .rgdt(LocalDateTime.now())
                .updt(LocalDateTime.now())
                .build();
    }

    public void addAddress(Address addr) {
        if (ObjectUtils.isEmpty(this.addrList)) {
            this.addrList = new ArrayList<>();
        }
        this.addrList.add(addr);
    }

    /**
     * 정보 수정
     */
    public void modifyUser(ReqUserInfo updateInfo) {
        this.userName = updateInfo.getUserName();
        this.password = StringUtils.isEmpty(updateInfo.getPassword())? this.password : updateInfo.getPassword();
    }

}
