package com.example.nyfappback.api.common.dto;

import com.example.nyfappback.api.common.type.ResultCode;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class NyfResult {

    @JsonIgnore
    HttpStatus httpStatus;
    @JsonIgnore
    ResultCode resultCode;
    Map<String, Object> result = null;

    public NyfResult(ResultCode result_code) {
        this.resultCode = result_code;
    }

    public NyfResult(HttpStatus httpStatus, ResultCode resultCode) {
        this.httpStatus = httpStatus;
        this.resultCode = resultCode;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus){
        this.httpStatus = httpStatus;
    }

    public void setResult(Map<String, Object> result) {
        this.result = result;
    }

    public static NyfResult getDefResult() {
        return new NyfResult(HttpStatus.OK, ResultCode.SUCCESS);
    }

    public static NyfResult getResult(ResultCode result_code) {
        return new NyfResult(result_code);
    }

    public void addObject(String key, Object value) {
        if(result == null) result = new HashMap<String, Object>();
        result.put(key, value);
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

    @JsonAnyGetter
    public Map<String, Object> getResult() {
        return result;
    }

    public ResponseEntity<?> getResponse(){
        if (result == null) {
            return new ResponseEntity<Object>(httpStatus);
        } else {
            return new ResponseEntity<Object>(result, httpStatus);
        }
    }
}
