package com.example.nyfappback.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig implements WebMvcConfigurer {

    /**
     * 스웨거 API 문서 생성
     * @author ny
     * @return Docket
     */
    @Bean
    public Docket swaggerAPI() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(this.swaggerInfo()) // 스웨거 정보 등록
                .select()
                .apis(RequestHandlerSelectors.any()) // 모든 controller 들이 있는 패키지를 탐색하여 API 문서 생성
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(true); // 기본으로 세팅되는 200, 401, 403, 404 메시지 표시
    }

    /**
     * 스웨거 정보
     * @author ny
     * @return ApiInfo
     */
    private ApiInfo swaggerInfo() {
        return new ApiInfoBuilder()
                .title("API Documentation")
                .description("Spring Boot API Documentation")
                .version("1.0.0")
                .build();
    }

    /** Swagger UI 를 Resource Handler 에 등록 */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("swagger-ui/index.html") .addResourceLocations("classpath:/META-INF/resources/");
    }

}

