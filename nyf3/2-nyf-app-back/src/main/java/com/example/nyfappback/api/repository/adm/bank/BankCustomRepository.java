package com.example.nyfappback.api.repository.adm.bank;

import com.example.nyfappback.api.model.adm.bank.BankVo;

import java.util.List;

public interface BankCustomRepository {
    List<BankVo> findBankListVo();
}
