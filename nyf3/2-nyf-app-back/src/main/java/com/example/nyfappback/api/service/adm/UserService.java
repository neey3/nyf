package com.example.nyfappback.api.service.adm;

import com.example.nyfappback.api.common.dto.NyfResult;
import com.example.nyfappback.api.common.type.ErrorType;
import com.example.nyfappback.api.common.util.ValidUtil;
import com.example.nyfappback.api.domain.entity.adm.Address;
import com.example.nyfappback.api.domain.entity.adm.User;
import com.example.nyfappback.api.model.adm.address.ReqAddressInfo;
import com.example.nyfappback.api.model.adm.address.RespAddressInfo;
import com.example.nyfappback.api.model.adm.user.ReqUserInfo;
import com.example.nyfappback.api.model.adm.user.RespUserDetail;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.example.nyfappback.api.repository.adm.address.AddressRepository;
import com.example.nyfappback.api.repository.adm.user.UserRepository;
import com.example.nyfappback.common.config.CustomModelMapperConfig;
import com.example.nyfappback.common.exception.BaseException;
import com.example.nyfappback.common.exception.NotFoundException;
import com.querydsl.core.QueryResults;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ObjectUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
@Service
public class UserService {
    private final CustomModelMapperConfig customModelMapper;
    private final UserRepository userRepository;
    private final AddressRepository addressRepository;


    // //////////////////////////////////// 회원 ////////////////////////////////////
    /**
     * 회원 목록 조회
     */
    public NyfResult findUserList(HttpServletRequest req) {
        NyfResult result = NyfResult.getDefResult();

        QueryResults<RespUserDetail> userList = userRepository.findUserInfo();

        result.addObject("result", true);
        result.addObject("data", userList.getResults());
        result.addObject("totalCnt", userList.getTotal());

        return result;
    }

    /**
     * 회원 상세 조회
     */
    public NyfResult findUserDetail(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();

        if (ValidUtil.isNullZero(body.getIdx())) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        RespUserDetail userDetail = userRepository.findUserDetail(body);

        result.addObject("data", userDetail);
        result.addObject("result", true);

        return result;
    }

    /**
     * 회원 등록
     */
    @Transactional
    public NyfResult registerUser(HttpServletRequest req, ReqUserInfo body) {
        NyfResult result = NyfResult.getDefResult();

        if (userRepository.findById(body.getIdx()).isPresent()) {
            result.addObject("result", false);
            result.addObject("errMsg", "이미 사용중인 ID 입니다.");
            return result;
        }

        User insertUser = User.createUser(body);
        userRepository.save(insertUser);

        result.addObject("result", true);
        result.addObject("msg", "등록 되었습니다.");
        return result;
    }

    /**
     * 회원 수정
     */
    @Transactional
    public NyfResult modifyUser(HttpServletRequest req, ReqUserInfo body) {
        NyfResult result = NyfResult.getDefResult();

        if (ValidUtil.isNullZero(body.getIdx())) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        User user = userRepository.findByUserIdx(body.getIdx()).orElseThrow(() -> new NotFoundException(ErrorType.BAD_REQUEST_ERROR));
        user.modifyUser(body);

        result.addObject("result", true);
        result.addObject("msg", "수정 되었습니다.");

        return result;
    }

    /**
     * 회원 삭제
     */
    @Transactional
    public NyfResult deleteUser(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();

        List<User> userList = userRepository.findUserListByIdxList(body.getIdxList());

        if (ObjectUtils.isEmpty(userList)) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        userList.forEach(userRepository::delete);

        result.addObject("result", true);
        result.addObject("msg", "삭제 되었습니다.");

        return result;
    }

    // //////////////////////////////////// 주소지 ////////////////////////////////////
    /**
     * 주소지 목록 조회  by userIdx
     */
    public NyfResult findAddressList(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();

        QueryResults<RespAddressInfo> addressList = addressRepository.findAddrListByUserIdx(body.getIdx());

        result.addObject("result", true);
        result.addObject("data", addressList.getResults());
        result.addObject("totalCnt", addressList.getTotal());

        return result;
    }

    /**
     * 주소지 등록
     */
    @Transactional
    public NyfResult registerAddress(HttpServletRequest req, ReqAddressInfo body) {
        NyfResult result = NyfResult.getDefResult();

        User user = userRepository.findUserByUserIdx(body.getUserIdx());

        Address insertAddress = Address.createAddress(body, user);
        addressRepository.save(insertAddress);

        result.addObject("result", true);
        result.addObject("msg", "등록 되었습니다.");
        return result;
    }

    /**
     * 주소지 수정
     */
    @Transactional
    public NyfResult modifyAddress(HttpServletRequest req, ReqAddressInfo body) {
        NyfResult result = NyfResult.getDefResult();

        Address address = addressRepository.findAddressByAddrIdx(body.getAddrIdx());

        if (ObjectUtils.isEmpty(address)) {
            throw new BaseException("존재하지 않는 주소지입니다. IDX 를 확인해 주세요.");
        }

        address.modifyAddress(body);

        result.addObject("result", true);
        result.addObject("msg", "수정 되었습니다.");

        return result;
    }

    /**
     * 주소지 삭제
     */
    @Transactional
    public NyfResult deleteAddress(HttpServletRequest req, ReqIdx body) {
        NyfResult result = NyfResult.getDefResult();

        List<Address> addressList = addressRepository.findAddrListByIdxList(body.getIdxList());

        if (ObjectUtils.isEmpty(addressList)) {
            throw new BaseException("IDX 를 확인해 주세요.");
        }

        addressList.forEach(addressRepository::delete);

        result.addObject("result", true);
        result.addObject("msg", "삭제 되었습니다.");

        return result;
    }

}
