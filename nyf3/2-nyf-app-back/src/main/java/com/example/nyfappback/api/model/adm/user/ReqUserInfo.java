package com.example.nyfappback.api.model.adm.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReqUserInfo {

    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "아이디")
    private String userId;

    @ApiModelProperty(value = "이름")
    private String userName;

    @ApiModelProperty(value = "(암호화된) 비밀번호")
    private String password;
}
