package com.example.nyfappback.api.model.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReqDisplay {
    @ApiModelProperty(value = "IDX")
    private Integer idx;

    @ApiModelProperty(value = "노출여부 ( 0 비노출, 1 노출 )")
    private Integer isDisplay;
}
