package com.example.nyfappback.common.handler;

import com.example.nyfappback.api.common.type.ErrorType;
import com.example.nyfappback.common.exception.*;
import lombok.extern.slf4j.Slf4j;
import io.undertow.util.BadRequestException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.UnexpectedTypeException;


@Slf4j
@RestControllerAdvice
public class CommExceptionHandler {

    // 잘못된 요청에 의한
    @ResponseStatus(HttpStatus.OK)
    @ExceptionHandler(BaseException.class)
    public ErrorResponse baseHandel(BaseException e) {
        log.error("BaseException : {} ", e.getErrMsg());
        return ErrorResponse.getFailResult(HttpStatus.OK.value(), e.getErrMsg(), false);
    }

    /**
     * catch로 잡아야하는 Server Exception
     **/
    @ExceptionHandler(ServerException.class)
    public ResponseEntity<?> handleServerException(ServerException e) {
        log.error("ServerException [{}] : {} ", e.getErrMsg() ,e.getMessage());
        return ResponseEntity.status(e.getEte() == null ? ErrorType.INTERNAL_SERVER_ERROR.getStatus() : e.getEte().getStatus())
                .body(ErrorResponse.getFailResult(e.getEte().getStatus(),e.getMessage()));
    }

    /**
     * 모든 Exception
     **/
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Exception.class)
    public ErrorResponse handleException(Exception e){
        log.error("Exception : {} ",e);
        return ErrorResponse.getFailResult(HttpStatus.INTERNAL_SERVER_ERROR.value(), ErrorType.INTERNAL_SERVER_ERROR.getErrMsg());
    }


    @ResponseStatus(HttpStatus.UNAUTHORIZED)
    @ExceptionHandler(UnauthorizedException.class)
    public ErrorResponse handleNotUserException(UnauthorizedException e){
        log.error("NotUserException: {}", e.getMessage());
        return ErrorResponse.getFailResult(HttpStatus.UNAUTHORIZED.value(), e.getMessage());
    }

     /**
     * 400 BAD_REQUEST 파라미터가 유효 하지 않는 경우
     **/
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadRequestException.class)
    public ErrorResponse handleBadRequestException(BadRequestException e){
        log.error("BadRequestException: {}", e.getMessage());
        return ErrorResponse.getFailResult(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    /**
     * 405 METHOD_NOT_ALLOWED 허용되지 않는 METHOD
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ErrorResponse handleNotSupportedException(HttpRequestMethodNotSupportedException e){
        log.error("handleNotSupportedException: {}", e.getMessage());
        return ErrorResponse.getFailResult(HttpStatus.BAD_REQUEST.value(), e.getMessage());
    }

    /**
     * NotFoundException (정보가 없는 경우)
     */
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<?> handleNotSupportedException(NotFoundException e) {
        log.error("NotFoundException: {}", e.getMessage());
        return ResponseEntity.status(e.getEte().getStatus()).body(ErrorResponse.getFailResult(e.getEte().getStatus(),e.getMessage()));
    }

    // Request Binding Error
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.OK)
    public ErrorResponse bindHandel(MethodArgumentNotValidException e){
        String errMsg = e.getBindingResult()
                .getAllErrors()
                .get(0)
                .getDefaultMessage();
        log.error("MethodArgumentNotValidException : " ,e);
        return ErrorResponse.getFailResult(HttpStatus.BAD_REQUEST.value(), errMsg, false);
    }

    // Request Binding Error
    @ExceptionHandler(UnexpectedTypeException.class)
    @ResponseStatus(HttpStatus.OK)
    public ErrorResponse bindHandel(UnexpectedTypeException e){
        log.error("UnexpectedTypeException {}", e);
        return ErrorResponse.getFailResult(HttpStatus.BAD_REQUEST.value(), "패턴에 맞지 않습니다.", false);
    }

}

