package com.example.nyfappback.api.model.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ReqIdx {
    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "IDX LIST")
    private List<Long> idxList;

    @ApiModelProperty(value = "Call Typ (null 일반, 1 실제 상세호출)")
    private Integer callType;
}
