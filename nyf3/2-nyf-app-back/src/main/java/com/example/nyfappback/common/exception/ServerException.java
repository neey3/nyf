package com.example.nyfappback.common.exception;

import com.example.nyfappback.api.common.type.ErrorType;
import lombok.Getter;


@Getter
public class ServerException extends RuntimeException{
    private ErrorType ete;
    private Exception exception;
    private String errMsg;

    public ServerException() {
        super(ErrorType.INTERNAL_SERVER_ERROR.getErrMsg());
        this.ete = ErrorType.INTERNAL_SERVER_ERROR;
    }

    public ServerException(ErrorType ete) {
        super(ete.getErrMsg());
        this.ete = ete;
    }

    public ServerException(ErrorType ete , Exception e) {
        super(e);
        this.ete = ete;
        this.exception = e;
    }

    public ServerException(Exception e){
        super(e);
        this.exception = e;
    }

    public ServerException(String errMsg, Exception e){
        super(e);
        this.errMsg = errMsg;
        this.exception = e;
    }
}
