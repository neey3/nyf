package com.example.nyfappback.api.domain.entity.adm;

import com.example.nyfappback.api.model.adm.admin.ReqAdminInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.util.StringUtils;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "admin")
@Builder
@DynamicInsert
@DynamicUpdate
public class Admin {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    @ApiModelProperty(value = "관리자 IDX")
    private Long idx;

    @ApiModelProperty(value = "아이디")
    @Column(name = "id")
    private String id;

    @ApiModelProperty(value = "관리자명")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "비밀번호")
    @Column(name = "password")
    private String password;

    @ApiModelProperty(value = "관리권한 (1 슈퍼, 2 일반)")
    @Column(name = "level")
    private Integer level;

    @ApiModelProperty(value = "로그인 key")
    @Column(name = "enc_key")
    private String encKey;

    @ApiModelProperty(value = "등록일")
    @Column(name = "rgdt")
    private LocalDateTime rgdt;

    @ApiModelProperty(value = "수정일")
    @Column(name = "updt")
    private LocalDateTime updt;


    /**
     * 로그인 시 EncKey Update
     */
    public void setLoginJwtEncKey(String encKey) {
        this.encKey = encKey;
    }

    /**
     * 관리자 등록을 위한 Entity 생성
     */
    public static Admin createAdmin(ReqAdminInfo body) {
        return Admin
                .builder()
                .id(body.getId())
                .name(body.getName())
                .password(body.getPassword())
                .level(body.getLevel() == null ? 1 : body.getLevel())
                .rgdt(LocalDateTime.now())
                .updt(LocalDateTime.now())
                .build();
    }

    /**
     * 관리자 정보 수정
     */
    public void modifyAdmin(ReqAdminInfo updateInfo) {
        this.name = updateInfo.getName();
        this.password = StringUtils.isEmpty(updateInfo.getPassword())? this.password : updateInfo.getPassword();
        this.level = updateInfo.getLevel();
        this.updt = LocalDateTime.now();
    }


}
