package com.example.nyfappback.api.repository.adm.bank;

import com.example.nyfappback.api.model.adm.bank.BankVo;
import com.querydsl.core.types.Projections;
import com.querydsl.jpa.impl.JPAQueryFactory;
import lombok.RequiredArgsConstructor;

import java.util.List;

import static com.example.nyfappback.api.domain.entity.adm.QBank.bank;

@RequiredArgsConstructor
public class BankCustomRepositoryImpl implements BankCustomRepository {
    private final JPAQueryFactory jpaQueryFactory;

    @Override
    public List<BankVo> findBankListVo() {
        return jpaQueryFactory
                .select(Projections.bean(
                        BankVo.class,
                        bank.code,
                        bank.bankName))
                .from(bank)
                .fetch();
    }
}
