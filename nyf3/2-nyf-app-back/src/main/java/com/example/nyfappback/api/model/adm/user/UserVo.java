package com.example.nyfappback.api.model.adm.user;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter
public class UserVo {

    private Long userIdx;
    private String userId;
    private String userName;
    private String password;
    private String userEmail;
    private String userPhone;
    private String postalCode;
    private String addr1;
    private String addr2;
    private String channel;
    private String encKey;
    private Integer withdrawalStatus;
    private String gender;
    private LocalDateTime rgdt;
    private LocalDateTime withdrawalDt;
    private LocalDateTime lastActionTime;
}
