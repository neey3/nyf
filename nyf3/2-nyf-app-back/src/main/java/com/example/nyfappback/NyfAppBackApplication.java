package com.example.nyfappback;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@SpringBootApplication
@EnableJpaAuditing
@WebListener
public class NyfAppBackApplication implements ServletContextListener {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(NyfAppBackApplication.class, args);
    }

}
