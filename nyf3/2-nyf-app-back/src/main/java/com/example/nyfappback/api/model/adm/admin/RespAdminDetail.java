package com.example.nyfappback.api.model.adm.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class RespAdminDetail {
    @ApiModelProperty(value = "IDX")
    private Integer idx;

    @ApiModelProperty(value = "아이디")
    private String  id;

    @ApiModelProperty(value = "이름")
    private String  name;

    @ApiModelProperty(value = "관리등급 (0 super, 1 일반)")
    private Integer level;

    @ApiModelProperty(value = "등록일")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime rgdt;

    @ApiModelProperty(value = "수정일")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updt;
}
