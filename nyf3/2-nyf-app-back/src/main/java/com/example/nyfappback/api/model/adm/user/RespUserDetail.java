package com.example.nyfappback.api.model.adm.user;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.querydsl.core.annotations.QueryProjection;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

@Getter
@Setter
@ToString
public class RespUserDetail {

    @ApiModelProperty(value = "회원 IDX")
    private Long userIdx;

    @ApiModelProperty(value = "아이디")
    private String userId;

    @ApiModelProperty(value = "이름")
    private String userName;

    @ApiModelProperty(value = "이메일")
    private String userEmail;

    @ApiModelProperty(value = "전화번호")
    private String userPhone;

    @ApiModelProperty(value = "유입경로 (PC, APP)")
    private String channel;

    @ApiModelProperty(value = "가입 상태 (0 탈퇴, 1 가입중)")
    private Integer withdrawalStatus;

    @ApiModelProperty(value = "성별 (0 남자, 1 여자)")
    private String gender;

    @ApiModelProperty(value = "가입일자")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime rgdt;

    @ApiModelProperty(value = "수정일자")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime updt;

    @ApiModelProperty(value = "탈퇴일자")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime withdrawalDt;

    @ApiModelProperty(value = "마지막 사용일자")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(shape=JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime lastActionTime;

    @ApiModelProperty(value = "우편번호")
    private String  postalCode;

    @ApiModelProperty(value = "주소1")
    private String addr1;

    @ApiModelProperty(value = "주소2")
    private String addr2;

    @QueryProjection
    public RespUserDetail(Long userIdx, String userId, String userName, String userEmail, String userPhone, String channel, Integer withdrawalStatus, String gender, LocalDateTime rgdt, LocalDateTime updt, LocalDateTime withdrawalDt, LocalDateTime lastActionTime, String postalCode, String addr1, String addr2) {
        this.userIdx = userIdx;
        this.userId = userId;
        this.userName = userName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.channel = channel;
        this.withdrawalStatus = withdrawalStatus;
        this.gender = gender;
        this.rgdt = rgdt;
        this.updt = updt;
        this.withdrawalDt = withdrawalDt;
        this.lastActionTime = lastActionTime;
        this.postalCode = postalCode;
        this.addr1 = addr1;
        this.addr2 = addr2;
    }
}
