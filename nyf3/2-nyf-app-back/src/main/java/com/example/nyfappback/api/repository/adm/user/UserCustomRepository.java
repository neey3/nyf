package com.example.nyfappback.api.repository.adm.user;

import com.example.nyfappback.api.domain.entity.adm.User;
import com.example.nyfappback.api.model.adm.user.RespUserDetail;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.querydsl.core.QueryResults;

import java.util.List;

public interface UserCustomRepository {
    public QueryResults<RespUserDetail> findUserInfo();
    public List<User> findUserListByIdxList(List<Long> idxList);

    public RespUserDetail findUserDetail(ReqIdx body);
    public User findUserByUserIdx(Long userIdx);
}
