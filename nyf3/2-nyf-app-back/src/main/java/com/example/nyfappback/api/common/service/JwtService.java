package com.example.nyfappback.api.common.service;

import com.example.nyfappback.api.common.dto.JwtToken;
import com.example.nyfappback.api.common.type.UserType;
import com.example.nyfappback.api.common.util.AesUtil;
import com.example.nyfappback.api.domain.entity.adm.Admin;
import com.example.nyfappback.api.domain.entity.adm.User;
import com.example.nyfappback.api.repository.adm.admin.AdminRepository;
import com.example.nyfappback.api.repository.adm.user.UserRepository;
import com.example.nyfappback.common.exception.UnauthorizedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class JwtService {
    private final AdminRepository adminRepository;
    private final UserRepository userRepository;

    @Transactional
    public String create(String key, JwtToken data, String subject) {
//        // 보안을 위해 HS256 -> RS256 으로 변경 -> RS256은 추후 적용.
//        // key 가 body에 포함되어있으면 안됨. -> DB 처리로 변경
//
//        String addiKey = generatePrivateKey();
//        Calendar cal = Calendar.getInstance();
//        cal.add(Calendar.MINUTE, 10);
//
//        String jwt = getJwtString(cal, key, data, addiKey, subject);
//
//        // 개인 식별 키 DB 저장 Logic 추가, 마지막 행동 시간 update ( Login 시 )
//        try {
//            if ("admin".equals(key)) {
//                Admin admin = adminRepository.findById(data.getIdx()).orElseThrow(() -> new UnauthorizedException());
//                admin.setLoginJwtEncKey(addiKey);
//            } else {
//                User user = userRepository.findById(data.getIdx()).orElseThrow(() -> new UnauthorizedException());
//                user.setLoginJwtEncKey(addiKey, LocalDateTime.now());
//            }
//
//        } catch (Exception e) {
//            log.info("..",e);
//            throw new UnauthorizedException();
//        }
//
//        // 한번 더 암호화
//        String plainText = data.getId() + "|||" + jwt + addiKey;
//        AesUtil aesUtil = new AesUtil();
//        try {
//            return aesUtil.encrypt(plainText);
//        } catch (Exception e) {
//            throw new UnauthorizedException();
//        }
        return null;
    }

    @Transactional
    public String resignToken(String key, JwtToken data, String subject) {
//        try {
//            Calendar cal = Calendar.getInstance();
//            String encKey = "";
//            if ("admin".equals(key)) {
//                Admin admin = adminRepository.findById(data.getId()).orElseThrow(() -> new UnauthorizedException());
//                cal.add(Calendar.HOUR, 2);
//                encKey = admin.getEncKey();
//            }
//            else {
//                User user = userRepository.findUser(UserType.FindTypeEnum.USERID.getType(), data.getId());
//                cal.add(Calendar.MINUTE, 10);
//                encKey = user.getEncKey();
//            }
//
//            String jwt = getJwtString(cal,key,data,encKey,subject);
//            // 한번 더 암호화
//            String plainText = data.getId() + "|||" + jwt + encKey;
//            AesUtil aesUtil = new AesUtil();
//
//            return aesUtil.encrypt(plainText);
//
//        } catch (Exception e) {
//            throw new UnauthorizedException();
//        }
        return null;
    }

    // Encrypt 키
    private static String generateEncKey(String addiKey) {
//        return (addiKey);
        return null;
    }

    // 개인 식별 키
    public String generatePrivateKey() {
//        StringBuffer temp = new StringBuffer();
//        Random rnd = new Random();
//        Date curDay = new Date();
//        rnd.setSeed(curDay.getTime());
//
//        for( int i = 0; i < 7; i++ ) {
//            int rIndex = rnd.nextInt(3);
//            switch (rIndex) {
//                case 0:
//                    // a-z
//                    temp.append((char) ((rnd.nextInt(26)) + 97));
//                    break;
//                case 1:
//                    // A-Z
//                    temp.append((char) ((rnd.nextInt(26)) + 65));
//                    break;
//                case 2:
//                    // 0-9
//                    temp.append((rnd.nextInt(10)));
//                    break;
//            }
//        }
//
//        return temp.toString();
        return null;
    }

    public boolean isUsable(String jwt, String claimKey) {
//        try {
//            // PC 요청인지 체크하기 위한 값 Setting
//            boolean isClient = "user".equals(claimKey) ? true : false;
//            String encKey = "";
//
//            String sString[] = new AesUtil().decrypt(jwt).split("\\|\\|\\|");
//
//
//            String id = "";
//            if( sString.length != 2 ) {
//                throw new UnauthorizedException();
//            } else {
//                id = sString[0];
//                jwt = sString[1];
//            }
//
//            if(isClient){
//                User user = userRepository.findUser(UserType.FindTypeEnum.USERID.getType(), id);
//                encKey = user.getEncKey();
//            }else{
//                Admin admin = adminRepository.findById(id).orElseThrow(()-> new UnauthorizedException());
//                encKey = admin.getEncKey();
//            }
//
//            // 뒤 7자리 제거 Logic 추가
//            String jwtBody = jwt.substring(0, jwt.length()-7);
//            String privateKey = jwt.substring(jwt.length()-7);
//
//            Jws<Claims> claims = Jwts.parser()
//                    .setSigningKey(this.generateEncKey(privateKey))
//                    .parseClaimsJws(jwtBody);
//
//            // DB 조회 하여 확인이 필요
//            @SuppressWarnings("unchecked")
//            Map<String, Object> adminMap = (LinkedHashMap<String, Object>)claims.getBody().get(claimKey);
//
//            if (adminMap == null) {
//                throw new UnauthorizedException();
//            }
//            else if (encKey == null || !encKey.equals(privateKey)) {
//                throw new UnauthorizedException();
//            }
//
//            return true;
//
//        } catch (Exception e) {
//            throw new UnauthorizedException();
//        }
        return true;
    }

    public static Map<String, Object> get(String key) {
//        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();
//        String jwt = request.getHeader("Authorization");
//        Jws<Claims> claims = null;
//        try {
//            String sString[] = new AesUtil().decrypt(jwt).split("\\|\\|\\|");
//
//            if( sString.length != 2 ) {
//                throw new UnauthorizedException();
//            } else {
//                jwt = sString[1];
//            }
//
//            String jwtBody = jwt.substring(0, jwt.length()-7);
//            String privateKey = jwt.substring(jwt.length()-7);
//            claims = Jwts.parser()
//                    .setSigningKey(generateEncKey(privateKey))
//                    .parseClaimsJws(jwtBody);
//        } catch (Exception e) {
//            throw new UnauthorizedException();
//        }
//
//        @SuppressWarnings("unchecked")
//        Map<String, Object> value = (LinkedHashMap<String, Object>)claims.getBody().get(key);
//
//        return value;
        return null;
    }

    public String getJwtString(Calendar cal, String key, JwtToken data, String addiKey,String subject) {
        return Jwts.builder()
                .setHeaderParam("type", "JWT")
                .setHeaderParam("regDate", System.currentTimeMillis())
                .setExpiration(cal.getTime())
                .setSubject(subject)
                .claim(key, data)
                .signWith(SignatureAlgorithm.HS256, this.generateEncKey(addiKey))
                .compact();
    }

}
