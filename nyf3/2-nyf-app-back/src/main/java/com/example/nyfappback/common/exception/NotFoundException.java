package com.example.nyfappback.common.exception;

import com.example.nyfappback.api.common.type.ErrorType;
import lombok.Getter;

@Getter
public class NotFoundException extends ClientBadRequestException{
    public NotFoundException(ErrorType ete, Exception exception) {
        super(ete, exception);
    }
    public NotFoundException(String message, ErrorType ete, Exception exception) {
        super(message, ete, exception);
    }
    public NotFoundException(ErrorType ete) {
        super(ete);
    }
}
