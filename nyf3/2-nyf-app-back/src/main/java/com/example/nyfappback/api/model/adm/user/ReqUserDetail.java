package com.example.nyfappback.api.model.adm.user;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.Min;

@Setter
@Getter
@ToString
public class ReqUserDetail {
    @ApiModelProperty(name="userIdx", value = "회원 IDX", required = true)
    @Min(value=1 , message="회원 IDX 를 확인해 주세요.")
    private Long userIdx;
}
