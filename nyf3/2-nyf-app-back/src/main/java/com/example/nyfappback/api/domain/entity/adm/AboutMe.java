package com.example.nyfappback.api.domain.entity.adm;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "about_myself")
public class AboutMe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idx")
    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "이름")
    @Column(name = "name")
    private String name;

    @ApiModelProperty(value = "이메일")
    @Column(name = "email")
    private String email;
}
