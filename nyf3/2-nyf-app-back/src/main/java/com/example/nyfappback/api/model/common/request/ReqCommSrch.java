package com.example.nyfappback.api.model.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.springframework.util.ObjectUtils;

import java.util.Optional;

@Setter
@Getter
public class ReqCommSrch {
    @ApiModelProperty(value = "검색 타입")
    protected Integer srchType;

    @ApiModelProperty(value = "검색 페이지")
    protected Integer srchPage;

    @ApiModelProperty(value = "검색 페이지당 수")
    protected Integer srchPageCnt;

    @ApiModelProperty(value = "시작 검색일")
    protected String srchStartDate;

    @ApiModelProperty(value = "종료 검색일")
    protected String srchEndDate;

    @ApiModelProperty(value = "검색 키워드")
    private String  srchKeyword;

    @ApiModelProperty(value = "검색 시작 날짜 00:00:00 시작")
    private String  srchDateStart;

    @ApiModelProperty(value = "검색 종료 날짜 23:59:59 종료")
    private String  srchDateEnd;

    public Integer getSrchPage() {
        if(!ObjectUtils.isEmpty(this.srchPage) && !ObjectUtils.isEmpty(this.srchPageCnt)){
            return Math.max((Optional.ofNullable(this.srchPage).orElse(1) - 1) * this.getSrchPageCnt(), 0);
        }
        return null;
    }

    public Integer getSrchPageCnt() {
        if(!ObjectUtils.isEmpty(this.srchPageCnt)){
            return Optional.ofNullable(this.srchPageCnt).get();
        }
        return null;
    }
}
