package com.example.nyfappback.api.controller.mng;

import com.example.nyfappback.api.common.dto.NyfResult;
import com.example.nyfappback.api.model.adm.address.ReqAddressInfo;
import com.example.nyfappback.api.model.adm.user.ReqUserInfo;
import com.example.nyfappback.api.model.common.request.ReqIdx;
import com.example.nyfappback.api.service.adm.UserService;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Log4j2
@RequestMapping(value = "/mng/user")
@RestController
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @ApiOperation(value = "[회원] 목록 조회", response = NyfResult.class, tags = "ADMIN/회원")
    @GetMapping("/list")
    public Object findUserList(HttpServletRequest req) {
        return userService.findUserList(req).getResponse();
    }

    @ApiOperation(value = "[회원] 상세 조회", response = NyfResult.class, tags = "ADMIN/회원")
    @PostMapping("/detail")
    public Object findUserDetail(HttpServletRequest req, @RequestBody ReqIdx body) {
        return userService.findUserDetail(req, body).getResponse();
    }

    @ApiOperation(value = "[회원] 등록", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/register")
    public Object registerUser(HttpServletRequest req, @RequestBody ReqUserInfo body) {
        return userService.registerUser(req, body).getResponse();
    }

    @ApiOperation(value = "[회원] 수정", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/modify")
    public Object modifyUser(HttpServletRequest req, @RequestBody ReqUserInfo body) {
        return userService.modifyUser(req, body).getResponse();
    }

    @ApiOperation(value = "[회원] 삭제", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/delete")
    public Object deleteUser(HttpServletRequest req, @RequestBody ReqIdx body) {
        return userService.deleteUser(req, body).getResponse();
    }

    @ApiOperation(value = "[회원/주소지] 목록 조회", response = NyfResult.class, tags = "ADMIN/회원")
    @PostMapping("/addr_list")
    public Object findAddressList(HttpServletRequest req, @RequestBody ReqIdx body) {
        return userService.findAddressList(req, body).getResponse();
    }

    @ApiOperation(value = "[회원/주소지] 등록", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/addr_register")
    public Object registerAddress(HttpServletRequest req, @RequestBody ReqAddressInfo body) {
        return userService.registerAddress(req, body).getResponse();
    }

    @ApiOperation(value = "[회원/주소지] 수정", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/addr_modify")
    public Object modifyAddress(HttpServletRequest req, @RequestBody ReqAddressInfo body) {
        return userService.modifyAddress(req, body).getResponse();
    }

    @ApiOperation(value = "[회원/주소지] 삭제", response = Map.class, tags = "ADMIN/회원")
    @PostMapping("/addr_delete")
    public Object deleteAddress(HttpServletRequest req, @RequestBody ReqIdx body) {
        return userService.deleteAddress(req, body).getResponse();
    }


}
