package com.example.nyfappback.api.repository.adm.admin;

import com.example.nyfappback.api.domain.entity.adm.Admin;
import com.example.nyfappback.api.model.adm.admin.RespAdminList;
import com.querydsl.core.QueryResults;

import java.util.List;

public interface AdminCustomRepository {
    public QueryResults<RespAdminList> findAllAdmin();
    public List<Admin> findAdminListByIdxList(List<Long> idxList);
}
