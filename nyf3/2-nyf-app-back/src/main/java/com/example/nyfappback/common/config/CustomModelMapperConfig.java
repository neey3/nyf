package com.example.nyfappback.common.config;

import com.example.nyfappback.api.domain.entity.adm.Admin;
import com.example.nyfappback.api.model.adm.admin.RespAdminDetail;
import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.modelmapper.ModelMapper;

@Configuration
public class CustomModelMapperConfig {
    private final ModelMapper modelMapper = new ModelMapper();

    public CustomModelMapperConfig() {
        modelMapper.getConfiguration().setSkipNullEnabled(true).setSkipNullEnabled(true);
        modelMapper.getConfiguration().setAmbiguityIgnored(true);

        modelMapper.createTypeMap(Admin.class, RespAdminDetail.class).addMappings(mapper -> {
            mapper.map(src -> src.getId(), RespAdminDetail::setId);
            mapper.map(src -> src.getName(), RespAdminDetail::setName);
            mapper.map(src -> src.getLevel(), RespAdminDetail::setLevel);
            mapper.map(src -> src.getRgdt(), RespAdminDetail::setRgdt);
            mapper.map(src -> src.getUpdt(), RespAdminDetail::setUpdt);
        });
    }

    @Bean
    public ModelMapper strictMapper(){
         modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
         return modelMapper;
    }

    @Bean
    public ModelMapper standardMapper(){
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STANDARD);
        return modelMapper;
    }

    @Bean
    public ModelMapper looseMapper(){
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);
        return modelMapper;
    }
}

