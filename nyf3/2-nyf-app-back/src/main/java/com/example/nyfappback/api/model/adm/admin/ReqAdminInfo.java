package com.example.nyfappback.api.model.adm.admin;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ReqAdminInfo {

    @ApiModelProperty(value = "IDX")
    private Long idx;

    @ApiModelProperty(value = "아이디")
    private String id;

    @ApiModelProperty(value = "(암호화된) 비밀번호")
    private String password;

    @ApiModelProperty(value = "관리권한 (1 슈퍼, 2 일반)")
    private Integer level;

    @ApiModelProperty(value = "관리자명")
    private String name;
}
