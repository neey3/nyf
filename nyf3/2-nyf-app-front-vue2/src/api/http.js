import axios from 'axios'

/** Creating an instance */
const instance = axios.create({
    instance: process.env.VUE_APP_API_BASE_URL,  // 데이터를 요청할 기본 주소
    timeout: 1000,
    // headers: {'X-Custom-Header': 'foobar'},
    // headers: { Authorization: store.state.token } // store에 있는 Token 사용
})

/** Add Interceptors */
//axios request 처리
function headers (data = false) {
    const token = sessionStorage.getItem('accessToken')
    const authHeader = token ? { 'Authorization': token } : {}
    const contentType = { 'Content-Type': data ? 'multipart/form-data' : 'application/json' }
    return {
        headers: {
            ...authHeader,
            ...contentType
        }
    }
    /*instance.interceptors.request.use(
        config => {
            return config.headers = {
                ...authHeader,
                ...contentType
            }
        },
        error => {
            console.log(error);
            return Promise.reject(error);
        }
    )*/
}
//axios response 처리
axios.interceptors.response.use(
    // 세션 유지를 위한 accessToken 갱신
    resp => {
        if (resp.headers['expire_token']) { sessionStorage.setItem('accessToken', resp.headers['expire_token']) }
        if (resp.status === 200) { return resp; }
        if (resp.status === 401) { window.location = '/' }  // 401 CLEAR token 정보를 반환하고 로그인 페이지로 이동
        return Promise.reject(resp)
    },
    err => {
        return Promise.reject(err)  // 오류정보 인터페이스에 반환
    }
);

/** export */
export default {
    getAxiosConfig (data = false, params = {}) {
        return {
            ...headers(data),
            params,
            validateStatus: (status) => { return status }
        }
    },
    getAxiosExportConfig (data = false, params = {}) {
        return {
            ...headers(data),
            params,
            validateStatus: (status) => { return status },
            responseType: 'blob'
        }
    },
    _get (url, body) {
        return new Promise((resolve, reject) => {
            if (body) {
                axios.get(instance + url, this.getAxiosConfig(false, body)).then(resp => {
                    console.log('axios.then(GET) => RESP::: ', resp)
                    resolve(resp)
                }).catch(e => {
                    console.log('axios.then(GET) => ERROR::: ', e)
                    reject(e)
                })
            } else {
                axios.get(instance + url, this.getAxiosConfig(false)).then(resp => {
                    console.log('axios.then(GET) => RESP::: ', resp.data)
                    resolve(resp)
                }).catch(e => {
                    console.log('axios.then(GET) => ERROR::: ', e)
                    reject(e)
                })
            }
        })
    },
    _post (url, body) {
        return new Promise((resolve, reject) => {
            axios.post(instance + url, body, this.getAxiosConfig(false)).then(resp => {
                console.log('axios.then(POST) => RESP::: ', resp)
                resolve(resp)
            }).catch(e => {
                console.log('axios.then(POST) => ERROR::: ', e)
                reject(e)
            })
        })
    },
    _multipartFile (url, data) {
        return new Promise((resolve, reject) => {
            axios.post(instance + url, data, this.getAxiosConfig(true)).then(resp => {
                console.log('axios.then(POST/multi-part) => RESP::: ', resp)
                resolve(resp)
            }).catch(e => {
                console.log('axios.then(POST/multi-part) => ERROR::: ', e)
                reject(e)
            })
        })
    },
    _getExportExcel (url, body) {
        return new Promise((resolve, reject) => {
            if (body) {
                axios.get(instance + url, this.getAxiosExportConfig(false, body)).then(resp => {
                    console.log('axios.then(GET_EXPORT) => RESP::: ', resp)
                    this._handlingBlob(resp)
                }).catch(e => {
                    console.log('axios.then(GET_EXPORT) => ERROR::: ', e)
                    reject(e)
                })
            } else {
                axios.get(instance + url, this.getAxiosExportConfig(false)).then(resp => {
                    console.log('axios.then(GET_EXPORT) => RESP::: ', resp.data)
                    this._handlingBlob(resp)
                }).catch(e => {
                    console.log('axios.then(GET_EXPORT) => ERROR::: ', e)
                    reject(e)
                })
            }
        })
    },
    _postExportExcel (url, body) {
        return new Promise((resolve, reject) => {
            axios.post(instance + url, body, this.getAxiosExportConfig(false)).then(resp => {
                console.log('axios.then(POST_EXPORT) => RESP::: ', resp)
                this._handlingBlob(resp)
                resolve(resp)
            }).catch(e => {
                console.log('axios.then(POST_EXPORT) => ERROR::: ', e)
                reject(e)
            })
        })
    },
    _handlingBlob (res) {
        let fileName = res.headers['content-disposition'].split('filename=')[1].split('"')[1];
        if (window.navigator && window.navigator.msSaveOrOpenBlob) { // IE11 and the legacy version Edge support
            console.log("IE & Edge");
            let blob = new Blob([res.data], { type: "text/html" });
            window.navigator.msSaveOrOpenBlob(blob, fileName);
        } else { // other browsers
            console.log("Other browsers");
            const bl = new Blob([res.data], {type: "text/html"});
            const a = document.createElement("a");
            a.href = URL.createObjectURL(bl);
            a.download = fileName;
            a.hidden = true;
            document.body.appendChild(a);
            a.click();
        }
    }
}
