export default api => {
    return {
        /**  */
        // 로그인
        login (body) {
            return api._post("/login", body)
        },
        // 이미지업로드
        uploadImage (body) {
            return api._multipartFile("/file/upload_image", body)
        },
        // 파일업로드
        uploadFile (body, part) {
            return api._multipartFile(`/file/upload_image?part=${part}`, body)
        },
        /**  */
        // 목록 get
        getList() {
            return api._get("/mng/admin/list")
        },
        // 목록 post
        getListInfo (body) {
            return api._post("/mng/admin/list", body)
        },
        // 상세조회
        getDetail (body) {
            return api._post("/mng/admin/detail", body)
        },
        // 등록
        reqRegister (body) {
            return api._post("/mng/admin/register", body)
        },
        // 수정
        reqModify (body) {
            return api._post("/mng/admin/modify", body)
        },
        // 삭제
        reqDel (body) {
            return api._post("/mng/admin/delete", body)
        },

    } //.return
} //.export
