import Vue from 'vue'
import VueRouter from 'vue-router'
import Main from '../views/Main.vue'

Vue.use(VueRouter)

const routes = [
    /**   */
    {
        path: '/',
        name: 'Main',
        component: Main
    },
    { //
        path: '/about',
        name: 'About',
        component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
    },
    /**   */
    { // 로그인
        path: '/login',
        name: 'LoginPage',
        component: () => import(/* webpackChunkName: "login" */ '../components/account/LoginPage.vue')
    },
    { // 회원가입
        path: '/sign-up',
        name: 'SignUp',
        component: () => import(/* webpackChunkName: "sign-up" */ '../components/account/SignUp.vue')
    },
    { // 아이디 찾기
        path: '/find-id',
        name: 'FindId',
        component: () => import(/* webpackChunkName: "find-id" */ '../components/account/FindId.vue')
    },
    { // 비밀번호 찾기
        path: '/find-pwd',
        name: 'FindPwd',
        component: () => import(/* webpackChunkName: "find-pwd" */ '../components/account/FindPwd.vue')
    },
    /**   */
    { //
        path: '/board-list',
        name: 'BoardList',
        component: () => import(/* webpackChunkName: "board" */ '../components/board/BoardList.vue')
    },
    { //
        path: '/board-register',
        name: 'BoardRegister',
        component: () => import(/* webpackChunkName: "board" */ '../components/board/BoardRegister.vue')
    },
    { //
        path: '/board-detail',
        name: 'BoardDetail',
        component: () => import(/* webpackChunkName: "board" */ '../components/board/BoardDetail.vue')
    },
    /**   */
    { //
        path: '/list',
        name: 'List',
        component: () => import(/* webpackChunkName: "list" */ '../views/List.vue')
    },
    { //
        path: '/pageList',
        name: 'PageList',
        component: () => import(/* webpackChunkName: "pageList" */ '../views/PageList.vue')
    },
    { //
        path: '/registration',
        name: 'Registration',
        component: () => import(/* webpackChunkName: "registration" */ '../components/Registration.vue')
    },
    { // 주소검색
        path: '/postCodePopup',
        name: 'PostCodePopup',
        component: () => import(/* webpackChunkName: "getAddress" */ '../views/popup/PostCodePopup')
    },
]

const router = new VueRouter({
    mode: "history",
    routes
})

export default router
