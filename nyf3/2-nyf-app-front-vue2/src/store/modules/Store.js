import axios from "@/api/apis";

const stores = {
    state: {
        userInfo: null,
        isLogin: false
    },
    mutations: {
        // loginSuccess(state, payload) {
        login(state, payload) {
            state.isLogin = true
            state.userInfo = payload
        },
        logout(state) {
            state.isLogin = false
            state.userInfo = null
            localStorage.removeItem("access_token")
        }
    },
    actions: {
        getAccountInfo({ commit }) {
            let token = localStorage.getItem("access_token")
            axios.get("/userinfo", {
                    headers: {
                        "X-AUTH-TOKEN": token
                    }
                })
                .then((response) => {
                    commit("loginSuccess", response.data.data)
                })
                .catch((error) => {
                    console.log(error)
                })
        }
    }
}

export default new Vuex.Store({
    stores
})

