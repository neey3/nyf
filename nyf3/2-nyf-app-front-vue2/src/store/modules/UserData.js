import http from '@/api/http'
import apis from '@/api/apis'
import { mutationType, actionType } from '@/store/modules/StoreType'

const Apis = apis(http)

/** state */
const state = {
  authToken: '',
  userData: {}
}
/** getters */
const getters = {
  userData: state => state.userData
}
/** mutations */
const mutations = {
  [ mutationType.SET_AUTH_TOKEN ] (state, accessToken) {
    console.log('[SET_AUTH_TOKEN] ', accessToken)
    state.accessToken = accessToken
    sessionStorage.accessToken = accessToken
  },
  [ mutationType.RESET_AUTH_TOKEN ] (state) {
    sessionStorage.removeItem('accessToken')
  },
  [ mutationType.RESET_USER_DATA ] (state) {
    sessionStorage.removeItem('userData')
  },
  [ mutationType.SET_USER_DATA ]  (state, params) {
    if (params) {
      console.log('[SET_USER_DATA] ', params)

      const jsonStr = JSON.stringify(params)
      state.userData = JSON.parse(jsonStr)
      sessionStorage.setItem('userData', jsonStr)
    }
  }
}
/** actions */
const actions = {
  [ actionType.SET_USER_DATA ] ({commit}, params) {
    commit(mutationType.SET_USER_DATA, params)
  },
  [ actionType.ACTION_LOGIN ] ({commit}, params) {
      let reqData = {
        id: params.username,
        password: params.password
      }
      return new Promise((resolve, reject) => {
        Apis.login(reqData).then(resp => {
          commit(mutationType.SET_USER_DATA, resp.data.info)
          commit(mutationType.SET_AUTH_TOKEN, resp.headers.authorization)
          resolve(resp)
        }).catch(error => {
          reject(error)
        })
      })
  },
  [ actionType.ACTION_LOGOUT ] ({commit}) {
    return new Promise((resolve, reject) => {
      console.log('[Logout] ')
      commit(mutationType.RESET_USER_DATA)
      commit(mutationType.RESET_AUTH_TOKEN)
      resolve()
    })
  }
}

// enhanceAccessToken 함수를 통해 헤더에 accessToken 을 디폴트로 박음 (안하면 새로고침시 토큰 날라감)
const enhanceAccessToken = () => {
  const {accessToken} = sessionStorage
  if (!accessToken) return;
  axios.defaults.headers.common['Authorization'] = `Bearer ${accessToken}`
}

export default {
  state,
  mutations,
  actions,
  getters,
  enhanceAccessToken
}
