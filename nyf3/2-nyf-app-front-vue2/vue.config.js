const { defineConfig } = require('@vue/cli-service')

module.exports = {
  // npm run build 시 파일 생성위치
  outputDir: "../src/main/resources/static",
  // index.html 생성위치
  indexPath: "../static/index.html",
  // 내장된 was 주소 (Port 번호는 Spring Server.port 와 같아야 함)
  devServer: {
    proxy: "http://localhost:8080",
  },
  chainWebpack: (config) => {
    const svgRule = config.module.rule("svg");
    svgRule.uses.clear();
    svgRule.use("vue-svg-loader").loader("vue-svg-loader");
  },
  // vue/multi-word-component-names err
  lintOnSave: false
};
