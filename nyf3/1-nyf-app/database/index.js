/*  index.js
   Node.js 서버를 생성한 후, 루트(/)와 /db/about-me 라는 URL 에 대해 간단한 처리를 하고 메시지를 돌려줌.
   특히 /db/about-me 가 엑세스되면 DB 내 tbl_about_myself 테이블에서 모든 정보를 꺼내와 응답으로 보낼 json 객체의 data 속성에 담아 응답 메시지로 보냄  */
// node index.js :  database 서버 실행 명령어
// http://localhost:3000/db/about-me
const sqlite3 = require('sqlite3')
const express = require('express')
const TYPE = require('./type.js')
const get = require('./get.js')
const post = require('./post.js')
const put = require('./put.js')
const initial = require('./initial.js')
const cors = require('cors')
const app = express()

app.disable('x-powered-by')
app.use(cors()) // cors 함수: 아무 파라미터 없이 사용하면 모든 Cross Origin 요청을 허용함
app.use('/assets', express.static('assets'))

const PORT = 3000

let db = new sqlite3.Database('database.db', (err) => {
    if (!err) {
        // 초기화. DB 가 생성되고 초기값이 들어감
        initial.run(db, TYPE.about_me)
        initial.run(db, TYPE.resume)
        initial.run(db, TYPE.applications)
        initial.run(db, TYPE.notification)
        initial.run(db, TYPE.blog)
        initial.run(db, TYPE.accounts)
        initial.run(db, TYPE.board)
    }
})

app.use(express.json())  // req.json parsing
app.listen(PORT, () => {
    console.log(`DB ### Listening... ${PORT}`)
})

get.setup(app, db)
post.setup(app, db)
put.setup(app, db)
