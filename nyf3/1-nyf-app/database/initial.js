// 테이블 생성, 값 저장
const TYPE = require('./type.js')

function fn_about_me (db) {
    // db.run('DROP TABLE IF EXISTS tbl_about_myself')
    db.run(
        'CREATE TABLE IF NOT EXISTS tbl_about_myself (name text, email text, UNIQUE(name, email))'
        , (err) => {
            if (!err) {
                db.run(
                    "INSERT OR IGNORE INTO tbl_about_myself (name, email) VALUES ('NY', 'ny@gmail.com')"
                )
                console.log('[DB init] fn_about_me')
            } else {
                console.log('[DB init ## ERROR] fn_about_me, err: ', err)
            }
        }
    )
}

function fn_resume (db) {
    db.run(
        'CREATE TABLE IF NOT EXISTS tbl_my_resume (date DATE, title TEXT, content TEXT, url TEXT, UNIQUE(date, title))'
        , (err) => {
            if (!err) {
                const resume = [
                    {
                        date: '1992-02-26',
                        title: 'ㅊㅋㅊㅋ',
                        content: '따란~!!',
                        url: null
                    },
                    {
                        date: '1999-03-01',
                        title: '8yr',
                        content: '초딩~',
                        url: null
                    },
                    {
                        date: '2005-03-01',
                        title: '14yr',
                        content: '중학생~',
                        url: 'https://www.google.com/'
                    }
                ]
                resume.forEach(item => {
                    const query = `INSERT OR IGNORE INTO tbl_my_resume (date, title, content, url) VALUES ('${item.date}', '${item.title}', '${item.content}', '${item.url}')`
                    db.run(query)
                })
                console.log('[DB init] fn_resume')
            } else {
                console.log('[DB init ## ERROR] fn_resume, err: ', err)
            }
        }
    )
}

function fn_applications (db) {
    db.run(
        'CREATE TABLE IF NOT EXISTS tbl_applications (id INT, name TEXT, content TEXT, date DATE, platform TEXT, url TEXT, image TEXT, UNIQUE(name, date))'
        , (err) => {
            if (!err) {
                const applications = [
                    {
                        id: 1,
                        name: '미니게임천국',
                        content: '버튼 하나로만 하는 미니게임! 놓아놓아, 올라올라, 미끌미끌, 아푸아푸, 빙글빙글, 건너건너, 막아막아, 돌아돌아, 날려날려 등 다양한 게임 존재',
                        date: '2005-08-19',
                        platform: 'Android',
                        url: 'https://www.androidlist.co.kr/item/android-apps/243/com-com2us-minigameparadise-normal-freefull-google-global-android-common/',
                        image: 'http://localhost:3000/assets/minigame.png'
                    },
                    {
                        id: 2,
                        name: '액션퍼즐패밀리',
                        content: '퍼즐을 해서 최고점수를 기록하고 총합을 환산해서 랭크를 결정하세요! 최대 랭크는 "황금의 성!"',
                        date: '2007-01-01',
                        platform: 'Android',
                        url: 'https://www.androidlist.co.kr/item/android-apps/34060/com-com2us-puzzlefamily-up-freefull-google-global-android-common/',
                        image: 'http://localhost:3000/assets/actionpuzzle.png'
                    }
                ]
                applications.forEach(item => {
                    const query = `INSERT OR IGNORE INTO tbl_applications (id, name, content, date, platform, url, image) VALUES ('${item.id}', '${item.name}', '${item.content}', '${item.date}', '${item.platform}', '${item.url}', '${item.image}')`
                    db.run(query)
                })
                console.log('[DB init] fn_applications')
            } else {
                console.log('[DB init ## ERROR] fn_applications, err: ', err)
            }
        }
    )
}

function fn_notification(db) {
    db.run(
        'CREATE TABLE IF NOT EXISTS tbl_notification (id INTEGER PRIMARY KEY AUTOINCREMENT, content TEXT, expiration DATE, type TEXT)'
        , (err) => {
            if (!err) {
                let query = 'DELETE from tbl_notification'
                db.run(query)
                console.log('[DB init] fn_notification ::: delete before insert')

                query = `INSERT OR IGNORE INTO tbl_notification (content, expiration, type) VALUES ('[사이트 점검중] 일부 사용에 제약이 있을 수 있습니다.', '2099-12-31', 'warning')`
                db.run(query)
                console.log('[DB init] fn_notification')
            } else {
                console.log('[DB init ## ERROR] fn_notification, err: ', err)
            }
        }
    )
}

function fn_blog(db) {
    db.run(
        "ALTER TABLE tbl_blog ADD type TEXT DEFAULT 'html'"
        , (err) => {
            if (!err) {
                db.run("ALTER TABLE tbl_blog ADD type TEXT DEFAULT 'html'")
                console.log('[DB init] fn_blog ::: alter table')
            } else {
                console.log('[DB init] fn_blog ::: there is already exist column with this name: \'type\'')
            }
        }
    )

    db.run(
        "CREATE TABLE IF NOT EXISTS tbl_blog (id INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, date DATETIME DEFAULT (datetime('now', 'localtime')), post TEXT, UNIQUE(title, post))"
        , (err) => {
            if (!err) {
                query = `INSERT OR IGNORE INTO tbl_blog (title, post) 
                            VALUES ("Sample Post", "<p>This blog post shows a few different types of content that’s supported and styled with Bootstrap. Basic typography, images, and code are all supported. </p><hr /><blockquote><h2>Heading</h2><p> Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. </p><h3>Sub-heading</h3><p> Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </p><pre><code>Example code block</code></pre><p> Aenean lacinia bibendum nulla sed consectetur. Etiam porta sem malesuada magna mollis euismod. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa. </p><ul><li>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</li><li>Donec id elit non mi porta gravida at eget metus.</li><li>Nulla vitae elit libero, a pharetra augue.</li></ul><p> Donec ullamcorper nulla non metus auctor fringilla. Nulla vitae elit libero, a pharetra augue. </p><ol><li>Vestibulum id ligula porta felis euismod semper.</li><li> Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. </li><li>Maecenas sed diam eget risus varius blandit sit amet non magna.</li></ol><p> Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis. </p>")`
                db.run(query)

                query = `INSERT OR IGNORE INTO tbl_blog (title, post) 
                            VALUES ("Featured Post", "<p>This is some additional paragraph placeholder content. It has been written to fill the available space and show how a longer snippet of text affects the surrounding content. We'll repeat it often to keep the demonstration flowing, so be on the lookout for this exact same string of text.</p> <blockquote> <p>Longer quote goes here, maybe with some <strong>emphasized text</strong> in the middle of it.</p> </blockquote> <p>This is some additional paragraph placeholder content. It has been written to fill the available space and show how a longer snippet of text affects the surrounding content. We'll repeat it often to keep the demonstration flowing, so be on the lookout for this exact same string of text.</p>")`
                db.run(query)
                console.log('[DB init] fn_blog')
            } else {
                console.log('[DB init ## ERROR] fn_blog, err: ', err)
            }
        }
    )
}

// admin only
function fn_accounts(db) {
    // db.run('DROP TABLE IF EXISTS tbl_accounts')
    // console.log('[DB init] fn_accounts _delete')

    db.run(
        "CREATE TABLE IF NOT EXISTS tbl_accounts (id INTEGER PRIMARY KEY AUTOINCREMENT, email TEXT, password TEXT, date DATETIME DEFAULT (datetime('now', 'localtime')), grade TEXT, token TEXT)",
        (err) => {
            if (!err) {
                query = `INSERT OR IGNORE INTO tbl_accounts (id, email,password, grade, token) VALUES ( (SELECT id FROM tbl_accounts WHERE grade = 'owner'), 'ny@gmail.com', '1111', 'owner', null)`
                db.run(query)
                console.log('[DB init] fn_accounts')
            }
        }
    )
}

function fn_board(db) {
    db.run(
        "CREATE TABLE IF NOT EXISTS tbl_board (idx INTEGER PRIMARY KEY AUTOINCREMENT, title TEXT, contents TEXT, author TEXT, rgdt DATETIME DEFAULT (datetime('now', 'localtime')), updt DATETIME DEFAULT (datetime('now', 'localtime')))"
        , (err) => {
            if (!err) {
                // db.run("SELECT count(tbl_board.idx) FROM tbl_board")

                console.log('!!!!!!!!!!!!!!!!!!!!!!!!', db)
                // querys = [
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목1', '게시글 내용1', 'user01')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목2', '게시글 내용2', 'user02')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목3', '게시글 내용3', 'user03')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목4', '게시글 내용4', 'user04')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목5', '게시글 내용5', 'user05')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목6', '게시글 내용6', 'user06')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목7', '게시글 내용7', 'user07')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목8', '게시글 내용8', 'user08')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목9', '게시글 내용9', 'user09')",
                //     "INSERT OR IGNORE INTO tbl_board (TITLE, CONTENTS, AUTHOR) VALUES ('게시글 제목10', '게시글 내용10', 'user10')"
                // ]
                // querys.forEach(query => {
                //     db.run(query)
                //     // console.log('query ::::: ', query)
                // })
                console.log('[DB init] fn_board')
            } else {
                console.log('[DB init ## ERROR] fn_board, err: ', err)
            }
        }
    )
}


module.exports.run = function (db, type) {
    if (type == TYPE.about_me) {
        fn_about_me(db)
    } else if (type == TYPE.resume) {
        fn_resume(db)
    } else if (type == TYPE.applications) {
        fn_applications(db)
    } else if (type == TYPE.notification) {
        fn_notification(db)
    } else if (type == TYPE.blog) {
        fn_blog(db)
    } else if (type == TYPE.accounts) {
        fn_accounts(db)
    } else if (type == TYPE.board) {
        fn_board(db)
    }
}
