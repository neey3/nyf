// 업데이트 (이메일, 비밀번호)
// ny@gmail.com 라는 이메일이 있을 때만 이메일 업데이트 가능, 그 외에는 비밀번호만 업데이트 되도록

module.exports.setup = function (app, db) {
    app.put('/db/accounts/:email/:password/:oldpassword', (req, res, next) => {
        let result = {
            rsp: 'fail',
        }
        db.get(
            `SELECT * FROM tbl_accounts WHERE (email='${req.params.email}' OR email='ny@gmail.com') AND grade='owner'`,
            (err, row) => {
                if (!err && row) {
                    if (row['password'] != req.params.password) {
                        result.rsp = 'wrong_password'
                        result.temp = row
                        res.json(result)
                        console.log('[DB|put] wrong_password', JSON.stringify(result))
                    } else {
                        db.run(
                            `UPDATE tbl_accounts SET email='${req.params.email}', password='${req.params.oldpassword}' WHERE (email='${req.params.email}' OR email='ny@gmail.com') AND grade='owner' AND password='${req.params.password}'`,
                            (err1) => {
                                if (err1) {
                                    result.error = err1.message
                                    res.json(result)
                                    console.log('[DB|put] ERROR ##, err1: ', result.error)
                                } else {
                                    result.rsp = 'ok'
                                    result.temp = `UPDATE tbl_accounts SET email='${req.params.email}', password='${req.params.oldpassword}' WHERE (email='${req.params.email}' OR email='ny@gmail.com') AND grade='owner' AND password='${req.params.password}'`
                                    res.json(result)
                                    console.log('[DB|put] update_accounts', JSON.stringify(result))
                                }
                            }
                        )
                    }
                } else {
                    result.rsp = 'no_email'
                    res.json(result)
                    console.log('[DB|put] ERROR ##, no_email: ', JSON.stringify(result))
                }
            }
        )
    })
}
