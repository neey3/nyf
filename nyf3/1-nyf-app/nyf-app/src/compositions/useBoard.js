import { reactive, computed } from 'vue'

export const useBoard = () => {
    const state = reactive({ boards: []})
    const board_count = computed(() => state.boards.length)

    const SET_DATA = (data) => {
        state.boards = data
        console.log('state.boards ### ', state.boards)
    }
    const boards = (filter = null) => {
        if (filter) {
            return computed(() => state.boards.filter((i) => i.title == filter))
        }
        return computed(() => state.boards)
    }
    const setBoards = (data) => {
        if (data.length > 0) {
            SET_DATA(data)
        }
    }
    const getBoardDetail = () => {

    }
    return {
        boards,
        board_count,
        setBoards,
        getBoardDetail
    }
}

// export const useBoard = () => {
//     // idx , title , contents , author , rgdt , updt
//     const state = reactive({ title: null , contents: null , author: null , rgdt: null , updt: null})
//     const SET_DATA = (data) => {
//         state[data.key] = data.value
//     }
//
//     const setBoard = (data) => {
//         Object.keys(data).forEach((key) => {
//             if (Object.keys(state).find((skey) => skey === key)) {
//                 SET_DATA({ key, value: data[key] })
//             }
//         })
//     }
//     return {
//         setBoard
//     }
// }
