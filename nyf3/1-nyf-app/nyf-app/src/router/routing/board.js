import BoardList from '/@components/board/BoardList.vue'
import BoardDetail from '/@components/board/BoardDetail.vue'
import BoardWrite from '/@components/board/BoardWrite.vue'

const board = [
    {
        path: '/board/list',
        name: 'BoardList',
        component: BoardList
    },
    {
        path: '/board/detail',
        name: 'BoardDetail',
        component: BoardDetail
    },
    {
        path: '/board/write',
        name: 'BoardWrite',
        component: BoardWrite
    },

]

export default board
