// // 사용자 프로필과 관련된 내용을 about_me 객체로 export.
// export const about_me = {
//     // Vuex 의 namespaced module.
//     // 모듈이 독립적으로 재사용되기 원할 경우 true 로 사용. (Store 를 namespace 단위로 나누어 각 스토어의 관심사를 분리)
//     namespaced: true,
//     // state 함수: 저장할 데이터들의 객체를 반환.
//     state: () => ({
//         name: null,
//         email: null,
//         resume: []
//     }),
//     // 전역 상태로부터 파생되는 값을 관리
//     getters: {
//         user_data: (state) => {
//             return {
//                 name: state.name,
//                 email: state.email,
//                 resume: state.resume
//             }
//         }
//     },
//     // 전역 상태를 동기적으로 변경하는 함수 관리. store 내 원본 데이터의 변형할 수 있는 유일한 속성
//     mutations: {
//         SET_DATA (state, data) {
//             state[data.key] = data.value
//         }
//     },
//     // 전역 상태를 비동기적으로 변경하는 함수 관리. 외부 컴포넌트에서 호출하는 함수의 집합을 가지고 있음
//     // 데이터 변형이 필요할 경우, 외부 컴포넌트들은 이 actions 내 함수들을 호출하고, actions 는 mutations 를 호출하여 데이터를 변형한다.
//     actions: {
//         setAboutMeData: ({ commit, state }, data) => {
//             Object.keys(data).forEach(key => {
//                 if (Object.keys(state).find((skey) => skey == key)) {
//                     commit('SET_DATA', { key: key, value: data[key] })
//                 }
//             })
//         }
//     }
// }
