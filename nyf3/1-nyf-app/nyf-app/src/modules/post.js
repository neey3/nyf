import useAxios from '/@utils/axios'
const { axiosPost, axiosPut } = useAxios()
import md5 from 'js-md5'

export default function () {

    const modifyPost = (email, password, oldpassword) =>
        new Promise((resolve, reject) => {
            // const enc_pw = password == '1111' ? password : password  // md-5 적용 전에는 해싱되기 전이므로
            const enc_pw = password == '1111' ? password : md5(password)  // md-5 적용 전에는 해싱되기 전이므로
            axiosPut(
                `/db/accounts/${md5(email)}/${enc_pw}/${md5(oldpassword)}`,
                {},
                (data) => {
                    resolve(data)
                },
                (data) => {
                    reject(data)
                }
            )
        })

    return {
        modifyPost
    }
}
