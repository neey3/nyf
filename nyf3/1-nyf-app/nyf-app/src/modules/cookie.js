const setCookie = (key, value, days = 1) => {
    const date = new Date()
    date.setHours(24 * days - date.getTimezoneOffset() / 60, 0, 0, 0)
    const expires = date.toUTCString()
    document.cookie = `${key}=${value};expires=${expires};path=/`
}

const getCookie = (key) => {
    const value = document.cookie
        .split(';')   // 쿠키는 일반적으로 세미콜론을 통해 키-값의 쌍이 분리되므로,
        .find((i) =>                  // 세미콜론을 이용해 '이름=값' 과 같은 문자열을 찾아내고,
            i.trim().startsWith(key + '=')  // '이름=' 뒤의 값을 불러오면 쿠키 값을 얻을 수 있음
        )

    if (!!value) {
        return value.trim().substring(key.length + 1)
    }

    return null
}

export { setCookie, getCookie }
