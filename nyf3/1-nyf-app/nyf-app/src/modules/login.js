import useAxios from '/@utils/axios'
const { axiosPost, axiosPut } = useAxios()
import md5 from 'js-md5'

export default function () {
    // md5  // email 및 pwd 와 관련된 모든 변수를 md5 함수에 넣어주면 md5 해시값이 나옴
    // 이메일과 토큰을 보내서 현재 관리자 접속 여부 확인 (≒세션)
    const checkToken = (email, token) =>
        new Promise((resolve, reject) => {
            axiosPost(
                // `/db/accounts/check-token/${email}/${token}`,
                `/db/accounts/check-token/${md5(email)}/${token}`,
                {},
                (data) => {
                    resolve(data)
                },
                (data) => {
                    reject(data)
                }
            )
        })
    // 이메일, 현재 비밀번호, 새로운 비밀번호로 정보 업데이트
    const updatePassword = (email, password, oldpassword) =>
        new Promise((resolve, reject) => {
            // const enc_pw = password == '1111' ? password : password  // md-5 적용 전에는 해싱되기 전이므로
            const enc_pw = password == '1111' ? password : md5(password)  // md-5 적용 전에는 해싱되기 전이므로
            axiosPut(
                // `/db/accounts/${email}/${password}/${oldpassword}`,
                `/db/accounts/${md5(email)}/${enc_pw}/${md5(oldpassword)}`,
                {},
                (data) => {
                    resolve(data)
                },
                (data) => {
                    reject(data)
                }
            )
        })
    // 이메일과 비밀번호로 로그인 (로그인되면 새로운 토큰값을 돌려받음)
    const login = (email, password) =>
        new Promise((resolve, reject) => {
            axiosPost(
                // `/db/accounts/login/${email}/${password}`,
                `/db/accounts/login/${md5(email)}/${md5(password)}`,
                {},
                (data) => {
                    resolve(data)
                },
                (data) => {
                    reject(data)
                }
            )
        })


    return {
        checkToken,
        updatePassword,
        login
    }
}
