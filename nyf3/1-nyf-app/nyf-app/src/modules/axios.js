import { axios } from '@bundled-es-modules/axios'
import { ref } from 'vue'

export default function () {
    const BASE_URL = 'http://localhost:3000'
    const communicating = ref(false)
    const creatURL = (url) => {
        return url.startsWith('http') ? url : BASE_URL + url
    }

    const checkResult = (resp, onSuccess, onFailed) => {
        communicating.value = false
        if (resp.status == 200 && resp.data.rsp == 'ok') {
            if (onSuccess) {
                onSuccess(resp.data)
            }
        } else {
            // 서버에서 에러 메시지를 받아 처리
            if (onFailed) {
                onFailed(resp.data)
            }
        }
    }

    // API 함수
    const axiosGet = (URL, onSuccess = null, onFailed = null) => {
        const final_URL = URL.startsWith('http') ? URL : BASE_URL + URL
        axios.get(final_URL).then(resp => {
            if (resp.status == 200 && resp.data.rsp == 'ok') {
                if (onSuccess) {
                    onSuccess(resp.data)
                } else {
                    if (onFailed) {
                        onFailed(resp.data)
                    }
                }
            }
        })
    }
    const axiosPost = async (url, data, onSuccess = null, onFailed = null) => {
        communicating.value = true
        axios.post(creatURL(url), data).then((resp) => {
            checkResult(resp, onSuccess, onFailed)
        })
    }
    const axiosPut = async (url, data, onSuccess = null, onFailed = null) => {
        communicating.value = true
        axios.put(creatURL(url), data).then((resp) => {
            checkResult(resp, onSuccess, onFailed)
        })
    }

    return {
        axiosGet,
        axiosPost,
        axiosPut,
        communicating
    }
}
