import { inject } from 'vue'

export const useFilter = () => {
    // const today = new Date().toISOString().split('T')[0] // 오늘이 며칠인지 ==> main.js 에 provide 로
    const today = inject('today')

    // 1)최근 날짜의 데이터가 더 위(앞)에 나오게, 2)같은 날이라면 뒤늦게 입력한 데이터가 더 위에 나오게
    const fnSort = (a, b) => {
        const a_date = Date.parse(a.date)
        const b_date = Date.parse(b.date)
        if (a_date > b_date) {
            return 1
        } else if (a_date < b_date) {
            return 0
        } else {
            return a.id - b.id
        }
    }

    const getPendingTodos = (todos) => {
        return todos.value
            .filter(todo => {
                todo.date < today && !todo.completed  // 날짜가 지났지만 아직 완료되지 않은 할일이 남은 항목
            })
            .slice()  // 정해진 구간의 배열값들을 뽑아내어 복사하여 새로운 배열을 만들어줌. (아무 인자가 없으면 전체를/인자가 1개면 해당 인덱스 값부터 마지막까지/인자가 2개면 시작지점부터 마지막 전까지 복사함)
            .sort(fnSort)
    }
    // 해야할 일
    const getActiveTodayTodos = (todos) => {
        return todos.value
            .filter(todo => {
                todo.date === today && !todo.completed
            })
            .slice()
            .sort(fnSort)
    }
    // 완료한 일
    const getCompletedTodayTodos = (todos) => {
        return todos.value
            .filter(todo => {
                todo.date === today && todo.completed
            })
            .slice()
            .sort(fnSort)
    }
    // 오늘의 모든 기록
    const getAllTodayTodos = (todos) => {
        return getActiveTodayTodos(todos)
            .concat(getCompletedTodayTodos(todos))
            .slice()
            .sort(fnSort)
    }
    // 모든 작업
    const getAllTodos = (todos) => {
        return todos.value
            .slice()
            .sort(fnSort)
    }

    return {
        getPendingTodos,
        getActiveTodayTodos,
        getCompletedTodayTodos,
        getAllTodayTodos,
        getAllTodos
    }
}
