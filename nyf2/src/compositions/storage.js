// Storage 모듈 : localStorage 로부터 todos 배열에 들어올 데이터를 불러옴
/*  reactive: 객체에 반응성을 더해주는 함수
    toRefs: 객체의 내부 속성들 모두에게 반응성을 더해줌
*/
import { reactive, toRefs } from 'vue'

export const useStorage = () => {
    const KEY = 'my-todo-list'
    const storage_obj = reactive({ storage_id: 0 })
    const loadTodos = (initTodos) => {
        let temp_todos = JSON.parse(localStorage.getItem(KEY) || '[]')
        temp_todos.forEach((todo, idx) => {
            todo.id = idx
        })
        storage_obj.storage_id = temp_todos.length
        initTodos(temp_todos)
    }
    const saveTodos = (todos) => {
        localStorage.setItem(KEY, JSON.stringify(todos.value))
    }
    return {
        ...toRefs(storage_obj),
        loadTodos,
        saveTodos
    }
}
/*  localStorage :
   -윈도우 오브젝트의 읽기전용 속성. 데이터를 브라우저에서 읽어오거나 저장할 수 있게 해줌.
   -데이터를 저장하고 불러오기 위한 setItem/getItem 함수를 제공하며, 키와 값 형식으로 데이터를 저장함
   -이밖에, 모든 데이터를 삭제할 수 있는 clear 함수, 특정한 키의 값을 삭제할 수 있는 removeItem 함수 有
*/
/*  useStorage: 총 4개의 변수를 가짐
   1) KEY: localStorage 에서 데이터를 저장할 key
   2) storage_obj: 일정 리스트를 가질 todos 속성과 신규 id 를 책정할 수 있는 storage_id 속성을 가진 객체
   3) loadTodos: localStorage 로부터 데이터를 불러오는 함수
   4) saveTodos: localStorage 로 데이터를 저장하는 함수
*/
