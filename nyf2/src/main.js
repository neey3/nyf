import { createApp } from 'vue'
// import './style.css'
import App from './App.vue'

// Bootstrap
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap/dist/js/bootstrap.esm.min.js'  // ES Module 로 컴파일된 것을 사용해야 하므로 import!

const app = createApp(App)
app.provide('today', new Date().toISOString().split('T')[0]) // 오늘을 나타내는 문자열. 같은 값이 여러 곳에서 쓰일 것이므로 지역변수 대신 변수를 provide 하여 > 모든 컴포넌트에서 today 를 inject 해서 사용할 수 있게 함.
app.mount('#app')
