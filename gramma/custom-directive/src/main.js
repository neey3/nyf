import { createApp, defineComponent } from 'vue'

/** */
const app = createApp({
    template: `
        <div style="height; 1000px">
            <p v-noti:top="100">고정 자리에 표시하기</p>
        </div>
    `
})
app.directive('noti', (el, binding, vnode, preNode) => {
    el.style.position = 'fixed'
    el.style[binding.arg || 'top'] = binding.value + 'px'
})

/** */
// directive 함수로 사용자가 정의한 디렉티브 사용.
app.directive('focus', {
    // 엘리먼트가 돔에 마운트/업데이트 등이 됐을 때의 액션을 작성
    mounted(el) {
        el.focus()
    },
    updated(el) {
        el.focus()
    }
})
// 마운트,업데이트 모두 적용되게 하고 싶을 경우, 아래와 같이
// 훅 이름을 지정하지 않은 무기명 함수를 선언하면 같은 코드를 두번 적지 않아도 된다.
app.directive('focus', (el) => {
    el.focus()
})


app.mount('#app')
