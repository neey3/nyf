import { createApp, ref } from 'vue'
import './style.css'

// 컴포넌트 생성
// 애플리케이션 인스턴스 app 생성
const app = createApp({})

/** app 생성 */
// 신규 컴포넌트를 app 에 등록
app.component('counter', {
    // provide 값을 불러서 사용 가능
    inject: ['counter_header'],
    setup() {
        const count = ref(0)
        return { count }
    },
    template: `
        <span>{{ counter_header }}</span>
        <button @click="count++" v-text="count"></button>
        <br />
    `
})

/** #app 엘리먼트에 app 을 장착 */
app.config.globalProperties.title = 'Counter 컴포넌트'
app.provide('counter_header', 'Counter ')
app.mount('#app')
