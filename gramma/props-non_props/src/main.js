import { createApp, ref } from 'vue'
import './style.css'

// 컴포넌트 생성
// 애플리케이션 인스턴스 app 생성
const app = createApp({})

/** Props */
app.component('PropsComponent', {
    props: {
        title: {
            type: String,   // 데이터 타입을 정의
            required: true, // props 가 들어오지 않으면 콘솔에 경고를 찍음
            validator: function (value) {   // 잘못 들어온 인자를 검사하여 콘솔창에 경고 찍음
                return typeof value == String
            }
        },
        data: {
            type: Object,
            default: () => {
                return []
            }
        }
    },
    setup() {
        const count = ref(0)
        return { count }
    },
    template: `
      <h5 v-text="title" />
      <span v-for="i in data" :key="i">{{ i }}</span>
    `
})

/** Non-Props */
// props 나 emits 옵션에 정의되지 않은 컴포넌트 속성.
// 스크립트 코드에서 $attrs 로 접근 가능함
app.component('NonPropsComponent', {
    // setup 함수의 context 인자에서 Non-Prop 속성 에 접근할 수 있다.
    setup(props, context) {
        const msg = context.attrs.msg
        return { msg }
    },
    template: `
      <NonPropsComponent3></NonPropsComponent3>
    `
})
app.component('NonPropsComponent2', {
    inheritAttrs: false, // false: 자식 노드에서 Non-Props 속성($attrs)을 상속하지 않음. (= Component2는 자식컴포넌트인 3d에게 값(msg)을 상속하지 X)
    setup(props, context) {
        const msg = context.attrs.msg
        return { msg }
    },
    template: `
      <NonPropsComponent3></NonPropsComponent3>
    `
})
app.component('NonPropsComponent3', {
    template: `
        <div>
            <p>@NonPropsComponent: {{ $attrs.msg }}</p>
        </div>
    `
})

/** #app 엘리먼트에 app 을 장착 */
app.mount('#app')
