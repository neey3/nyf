import { createApp } from 'vue'

const mx = {
    data() {
        return {
            total: 5,
            data: [1,2,3,4,5]
        }
    }
}

const app = createApp({
    mixins: [
        mx
        // Vue3 에서는 Mixins 보다는 컴포지션 함수로 대체 사용하는 것을 권장함!
    ],
    data() {
        return {
            total: 10
        }
    },
    template: `
      <p>TOTAL: {{ total }}</p>
      <!-- Mixins 는 컴포넌트보다 먼저 호출되므로, 출력값은 10 -->
    `
})

app.mount('#app')
